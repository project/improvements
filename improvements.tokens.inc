<?php

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 */
function improvements_token_info(): array {
  $info = [];

  // @TODO Remove after issue close https://www.drupal.org/project/token/issues/1198032
  $info['tokens']['current-page']['url-with-query'] = [
    'name' => t('URL with query'),
    'description' => t('The URL of the current page with query.'),
    'type' => 'url',
  ];

  $info['tokens']['random']['number_from_range'] = [
    'name' => t('Number from range'),
    'description' => t('Random number from range.'),
    'dynamic' => TRUE,
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function improvements_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];
  $token_service = \Drupal::token();

  // [current-page:*]
  if ($type == 'current-page') {
    $request = \Drupal::request();
    $language_manager = \Drupal::languageManager();

    $url_options = ['absolute' => TRUE];
    if (isset($options['langcode'])) {
      $url_options['language'] = $language_manager->getLanguage($options['langcode']);
    }

    foreach ($tokens as $name => $original) {
      /** @see token_tokens():758 */
      if ($name == 'url-with-query') {
        $url = NULL;

        try {
          $url = Url::createFromRequest($request)
            ->setOptions($url_options)
            ->setOption('query', $request->query->all());
        }
        catch (\Exception $e) {
          // Url::createFromRequest() can fail, e.g. on 404 pages.
          // Fall back and try again with Url::fromUserInput().
          try {
            $url = Url::fromUserInput($request->getPathInfo(), $url_options);
          }
          catch (\Exception $e) {
            // Instantiation would fail again on malformed urls.
          }
        }

        if (isset($url)) {
          $replacements[$original] = $url->toString();
        }

        $bubbleable_metadata->addCacheContexts(['url.path']);
      }
    }

    // Chained token relationships.
    if ($url_tokens = $token_service->findWithPrefix($tokens, 'url-with-query')) {
      $url = NULL;
      try {
        $url = Url::createFromRequest($request)
          ->setOptions($url_options)
          ->setOption('query', $request->query->all());
      }
      catch (\Exception $e) {
        // Url::createFromRequest() can fail, e.g. on 404 pages.
        // Fall back and try again with Url::fromUserInput().
        try {
          $url = Url::fromUserInput($request->getPathInfo(), $url_options);
        }
        catch (\Exception $e) {
          // Instantiation would fail again on malformed urls.
        }
      }

      // Add cache contexts to ensure this token functions on a per-path basis
      $bubbleable_metadata->addCacheContexts(['url.path']);
      $replacements += $token_service->generate('url', $url_tokens, ['url' => $url], $options, $bubbleable_metadata);
    }
  }

  // [random:*]
  if ($type == 'random') {
    // [random:number_from_range:*]
    if ($tokens_with_prefix = $token_service->findWithPrefix($tokens, 'number_from_range')) {
      foreach ($tokens_with_prefix as $arguments => $original) {
        [$min, $max] = array_map('intval', explode(',', $arguments));
        $replacements[$original] = random_int($min, $max);
      }
    }
  }

  return $replacements;
}
