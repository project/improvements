<?php

namespace Drupal\improvements_toolbar;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber MyEventSubscriber.
 */
class ImprovementsToolbarEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 10];
    return $events;
  }

  /**
   * KernelEvents::RESPONSE event callback.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    // Remove toolbar/toolbar library from page attachments
    $response = $event->getResponse();
    if (method_exists($response, 'getAttachments')) {
      $attachments = $response->getAttachments();
      if (isset($attachments['library'])) {
        $attachments['library'] = array_diff($attachments['library'], ['toolbar/toolbar']);
        $response->setAttachments($attachments);
      }
    }
  }

}
