<?php

namespace Drupal\improvements_title;

use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber MyEventSubscriber.
 */
class ImprovementsTitleEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::VIEW][] = ['onKernelView', 10];
    return $events;
  }

  /**
   * KernelEvents::VIEW event callback.
   */
  public function onKernelView(ViewEvent $event): void {
    // Remove #title from modal content
    $result = $event->getControllerResult();

    /** @see \Drupal\Core\Render\MainContent\ModalRenderer::renderResponse() */
    if (
      $event->getRequest()->query->get(MainContentViewSubscriber::WRAPPER_FORMAT) == 'drupal_modal' &&
      isset($result['#pre_render'])
    ) {
      foreach ($result['#pre_render'] as $key => $function) {
        if (is_array($function) && isset($function[1]) && $function[1] == 'buildTitle') {
          unset($result['#pre_render'][$key]);
        }
      }
    }

    $event->setControllerResult($result);
  }

}
