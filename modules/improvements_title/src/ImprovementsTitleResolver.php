<?php

namespace Drupal\improvements_title;

use Drupal\Core\Controller\TitleResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Replacement for title_resolver service.
 */
class ImprovementsTitleResolver extends TitleResolver {

  protected mixed $title = '';

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request, Route $route) {
    $title = $this->title;

    if (!$title) {
      $title = parent::getTitle($request, $route);
    }

    // Trigger hook_page_title_alter()
    \Drupal::moduleHandler()->alter('page_title', $title, $request, $route);

    return $title;
  }

  /**
   * Set page title.
   *
   * @param mixed $title
   */
  public function setTitle(mixed $title): void {
    $this->title = $title;
  }

}
