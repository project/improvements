<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function improvements_site_information_token_info(): array {
  $info = [];

  $site_third_party_settings_info = improvements_get_site_third_party_settings_info();
  foreach ($site_third_party_settings_info as $setting_name => $setting_info) {
    $info['tokens']['site'][$setting_name] = [
      'name' => $setting_info['title'],
      'description' => $setting_info['description'] ?? '',
    ];
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function improvements_site_information_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];

  // [site:*]
  if ($type == 'site') {
    $site_third_party_settings_info = improvements_get_site_third_party_settings_info();
    foreach ($tokens as $name => $original) {
      if (isset($site_third_party_settings_info[$name])) {
        $replacements[$original] = improvements_get_site_information($name);
      }
    }
  }

  return $replacements;
}
