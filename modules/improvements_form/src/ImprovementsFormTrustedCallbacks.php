<?php

namespace Drupal\improvements_form;

use Drupal\Core\Security\TrustedCallbackInterface;

class ImprovementsFormTrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'managedFilePreRender',
    ];
  }

  /**
   * "Managed file" element pre render callback.
   *
   * @see improvements_element_info_alter()
   */
  public static function managedFilePreRender(array $element): array {
    // Ability disable file auto-upload
    if (isset($element['upload']) && $element['upload']['#type'] == 'file') {
      if (isset($element['#autoupload']) && !$element['#autoupload']) {
        $element['upload']['#attributes']['class'][] = 'js-form-file--no-autoupload';
      }
    }

    // "Upload" button state
    // 1 - visible, 0 - removed, any other value - hidden
    if (isset($element['upload_button'], $element['#upload_button_state'])) {
      if ($element['#upload_button_state'] == 1) {
        $element['upload_button']['#attributes']['class'] = array_diff($element['upload_button']['#attributes']['class'], ['js-hide']);
      }
      elseif ($element['#upload_button_state'] == 0) {
        $element['upload_button']['#access'] = FALSE;
      }
    }

    return $element;
  }

}
