<?php

namespace Drupal\improvements_form\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class FormResetCommand implements CommandInterface {

  protected string $formSelector;

  protected string $removeErrors;

  /**
   * Command constructor.
   */
  public function __construct(string $form_selector, bool $remove_errors = TRUE) {
    $this->formSelector = $form_selector;
    $this->removeErrors = $remove_errors;
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    return [
      'command' => 'formReset',
      'selector' => $this->formSelector,
      'removeErrors' => $this->removeErrors,
    ];
  }

}
