<?php

namespace Drupal\improvements_form\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Url;

class HistoryReplaceStateCommand implements CommandInterface {

  protected mixed $url;

  /**
   * Command constructor.
   */
  public function __construct($url) {
    $this->url = $url;
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    $url = $this->url;
    if ($url instanceof Url) {
      $url = $url->toString();
    }

    return [
      'command' => 'historyReplaceState',
      'url' => (string)$url,
    ];
  }

}
