<?php

namespace Drupal\improvements_form\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class RemoveAjaxSettingsCommand implements CommandInterface {

  protected string $instanceId;

  /**
   * Command constructor.
   */
  public function __construct(string $instance_id) {
    $this->instanceId = $instance_id;
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    return [
      'command' => 'removeAjaxSettings',
      'instanceId' => $this->instanceId,
    ];
  }

}
