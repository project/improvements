<?php

namespace Drupal\improvements_form\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class PageReloadCommand implements CommandInterface {

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    return [
      'command' => 'pageReload',
    ];
  }

}
