<?php

namespace Drupal\improvements_form\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class RemoveClassCommand implements CommandInterface {

  protected string $selector;

  protected string $class_name;

  /**
   * Command constructor.
   */
  public function __construct(string $selector, string $class_name) {
    $this->selector = $selector;
    $this->class_name = $class_name;
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    return [
      'command' => 'invoke',
      'selector' => $this->selector,
      'method' => 'removeClass',
      'args' => [$this->class_name],
    ];
  }

}
