<?php

namespace Drupal\improvements_form\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class ConsoleLogCommand implements CommandInterface {

  protected mixed $message;

  /**
   * Command constructor.
   */
  public function __construct($message) {
    $this->message = $message;
  }

  /**
   * {@inheritDoc}
   */
  public function render(): array {
    return [
      'command' => 'consoleLog',
      'message' => $this->message,
    ];
  }

}
