<?php

namespace Drupal\improvements_form\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Url;

/**
 * @FormElement("jquery_autocomplete")
 */
class JqueryAutocomplete extends Textfield {

  /**
   *{@inheritDoc}
   */
  public function getInfo(): array {
    $info = parent::getInfo();

    $info['#route_name'] = '';
    $info['#route_parameters'] = [];
    $info['#route_options'] = [];
    $info['#process'][] = [static::class, 'processJqueryAutocomplete'];

    return $info;
  }

  /**
   * #pre_render callback.
   */
  public static function processJqueryAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $element['#attributes']['class'][] = 'form-jquery-autocomplete';
    $element['#attributes']['data-autocomplete-source'] = Url::fromRoute($element['#route_name'], $element['#route_parameters'], $element['#route_options'])->toString();
    $element['#attached']['library'][] = 'improvements_form/jquery-autocomplete';

    return $element;
  }

}
