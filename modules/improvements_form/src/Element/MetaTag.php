<?php

namespace Drupal\improvements_form\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * @RenderElement("meta_tag")
 *
 * @TODO Add test
 */
class MetaTag extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    return [
      '#pre_render' => [
        [static::class, 'preRender'],
      ],
    ];
  }

  /**
   * Element #pre_render callback.
   */
  public static function preRender(array $element): array {
    $element['tag'] = [
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => [
        'name' => $element['#name'],
        'content' => [
          '#plain_text' => $element['#content'],
        ],
      ],
    ];

    return $element;
  }

}
