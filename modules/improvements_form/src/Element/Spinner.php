<?php

namespace Drupal\improvements_form\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Number;

/**
 * @FormElement("spinner")
 */
class Spinner extends Number {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = static::class;
    return [
      '#min' => NULL,
      '#max' => NULL,
      '#pre_render' => [
        [$class, 'preRenderSpinner'],
      ],
      '#theme' => 'input__spinner',
    ] + parent::getInfo();
  }

  /**
   * Element #pre_render callback.
   */
  public static function preRenderSpinner(array $element): array {
    $element['#attributes']['type'] = 'number';
    Element::setAttributes($element, ['id', 'name', 'value', 'step', 'min', 'max', 'placeholder', 'size']);
    static::setAttributes($element, ['form-spinner']);

    $element['#attached']['library'][] = 'improvements_form/spinner';

    return $element;
  }

}
