(function ($, Drupal) {

  Drupal.behaviors.improvementsRangeSlider = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Init jQuery UI Slider widget
      context.querySelectorAll('.form-range-slider__slider:not(.ui-slider)').forEach(function (sliderElement) {
        var $sliderElement = $(sliderElement);
        var $sliderWrapper = $sliderElement.closest('.form-range-slider');
        var $minInput = $sliderWrapper.find('.form-range-slider__min');
        var $maxInput = $sliderWrapper.find('.form-range-slider__max');
        var min = parseFloat($minInput.attr('min'));
        var max = parseFloat($minInput.attr('max'));
        var step = parseFloat($minInput.attr('step'));

        // Show default values in placeholder
        $minInput.attr('placeholder', $minInput.attr('min'));
        $maxInput.attr('placeholder', $maxInput.attr('max'));

        $sliderElement.slider({
          range: true,
          min: min,
          max: max,
          step: step,
          // Default slider values
          values: [
            $minInput.val() !== '' ? $minInput.val() : $minInput.attr('min'),
            $maxInput.val() !== '' ? $maxInput.val() : $maxInput.attr('max'),
          ],
          // Set inputs value on slider slide
          slide: function (event, ui) {
            var $input = (ui.handleIndex == 0) ? $minInput : $maxInput;
            $input.val(ui.value);
          },
          // Trigger input "change" event after user stop sliding
          stop: function (event, ui) {
            var $input = (ui.handleIndex == 0) ? $minInput : $maxInput;
            $input.trigger('change');
            $input.trigger('input');
          },
        });

        // Change slider after inputs changed
        $minInput.on('input', function () {
          $sliderElement.slider('values', 0, this.value);
        });
        $maxInput.on('input', function () {
          if (this.value === '') {
            $sliderElement.slider('values', 1, $maxInput.attr('max'));
          }
          else {
            $sliderElement.slider('values', 1, this.value);
          }
        });
      });
    }
  };

})(jQuery, Drupal);
