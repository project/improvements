(function (Drupal) {

  function textareaKeyupCallback(event) {
    if (event.keyCode == 13) {
      var textarea = event.currentTarget;
      var textareaValue = textarea.value;
      var textareaCursorPosition = textarea.selectionStart;
      var spacesCount = 0;
      var currentChar;

      for (var i = textareaCursorPosition - 2; i >= 0; i--) {
        currentChar = textareaValue.charAt(i);
        if (currentChar == "\n") {
          break;
        }
        if (currentChar == ' ') {
          spacesCount++;
        }
        else {
          spacesCount = 0;
        }
      }

      if (spacesCount > 0) {
        var spaces = ' '.repeat(spacesCount);
        textarea.value = textareaValue.substring(0, textareaCursorPosition) + spaces + textareaValue.substring(textareaCursorPosition);
        textarea.selectionStart = textarea.selectionEnd = textareaCursorPosition + spacesCount;
      }
    }
  }

  // Safe leading spaces in textareas
  Drupal.behaviors.improvementsTextareaLeadingSpaces = {
    // Attach
    attach: function attach(context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      context.querySelectorAll('.form-textarea').forEach(function (textareaElement) {
        textareaElement.addEventListener('keyup', textareaKeyupCallback);
      });
    },

    // Detach
    detach: function detach(context, settings, trigger) {
      context.querySelectorAll('.form-textarea').forEach(function (textareaElement) {
        textareaElement.removeEventListener('keyup', textareaKeyupCallback);
      });
    }
  };

})(Drupal);
