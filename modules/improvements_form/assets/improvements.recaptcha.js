(function (Drupal) {

  Drupal.behaviors.improvementsRecaptcha = {
    attach: function attach(context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Fix recaptcha on dynamic loaded html
      if ('grecaptcha' in window && context !== document) {
        context.querySelectorAll('.g-recaptcha:empty').forEach(function (captchaElement) {
          grecaptcha.render(captchaElement, {...captchaElement.dataset});
        });
      }
    }
  };

})(Drupal);
