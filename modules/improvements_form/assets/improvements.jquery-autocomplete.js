(function ($, Drupal) {

  Drupal.behaviors.improvementsSimpleAutocomplete = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      once('jquery-autocomplete', '.form-jquery-autocomplete', context).forEach(function (element) {
        var $element = $(element);

        $element.autocomplete({
          source: element.dataset.autocompleteSource,
        });
      });
    }
  };

})(jQuery, Drupal);
