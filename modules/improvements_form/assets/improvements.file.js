(function (Drupal) {

  Drupal.behaviors.improvementsFile = {
    attach: function attach(context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Disable file autoupload
      // @see Drupal.behaviors.fileAutoUpload.attach()
      context.querySelectorAll('.js-form-file--no-autoupload').forEach(fileInput => {
        Drupal.behaviors.fileAutoUpload.detach(fileInput.parentElement, settings, 'unload');
        Drupal.behaviors.fileButtons.detach(context, settings, 'unload');
      });
    }
  };

  Drupal.file.disableFields = function () {};

})(Drupal);
