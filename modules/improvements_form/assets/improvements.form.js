(function (Drupal) {

  Drupal.behaviors.improvementsForm = {
    attach: function attach(context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Trigger "delayedinput" event after user change input
      once('delayedinput', '\
        input[type="text"],\
        input[type="number"],\
        input[type="email"],\
        textarea\
      ', context).forEach(function (element) {
        var timeout = element.dataset.inputTimeout ? element.dataset.inputTimeout : 600;
        var timer;

        var triggerCallback = function () {
          element.dispatchEvent(new CustomEvent('delayedinput'));
          timer = null;
        };

        element.addEventListener('input', function (event) {
          clearTimeout(timer);
          timer = setTimeout(triggerCallback, timeout);
        });

        element.addEventListener('blur', function (event) {
          if (timer) {
            clearTimeout(timer);
            triggerCallback();
          }
        });
      });
    }
  };

})(Drupal);
