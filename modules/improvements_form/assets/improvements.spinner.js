(function ($, Drupal) {

  Drupal.behaviors.improvementsSpinner = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Init jQuery UI Spinner widget
      context.querySelectorAll('input.form-spinner:not(.ui-spinner-input)').forEach(function (spinnerElement) {
        $(spinnerElement).spinner({
          // Trigger "input" event when spinner changed
          // Used "spin" event because "stop" event triggered when element losing focus.
          spin: function () {
            this.dispatchEvent(new Event('input'));
          },
          icons: {
            up: '',
            down: '',
          },
          classes: {
            'ui-spinner': '',
            'ui-spinner-down': '',
            'ui-spinner-up': '',
          },
        });
      });
    }
  };

})(jQuery, Drupal);
