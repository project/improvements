(function (Drupal) {

  /**
   * Command "historyReplaceState".
   */
  Drupal.AjaxCommands.prototype.historyReplaceState = function (ajax, response, status) {
    history.replaceState(null, null, response.url);
  };

  /**
   * Command "pageReload".
   */
  Drupal.AjaxCommands.prototype.pageReload = function (ajax, response, status) {
    location.reload();
  };

  /**
   * Command "consoleLog".
   */
  Drupal.AjaxCommands.prototype.consoleLog = function (ajax, response, status) {
    console.log(response.message);
  };

  /**
   * Command "formReset".
   */
  Drupal.AjaxCommands.prototype.formReset = function (ajax, response, status) {
    var form = document.querySelector(response.selector);

    form.reset();
    form.dispatchEvent(new Event('reset'));

    if (response.removeErrors) {
      // Remove system messages
      form.querySelectorAll('[data-drupal-messages]').forEach(function (messageElement) {
        messageElement.remove();
      });

      // Remove "error" class from elements
      form.querySelectorAll('.error').forEach(function (inputElement) {
        inputElement.classList.remove('error');
      });
    }
  };

  /**
   * Command "removeAjaxSettings".
   */
  Drupal.AjaxCommands.prototype.removeAjaxSettings = function (ajax, response, status) {
    if (response.instanceId in window.drupalSettings.ajax) {
      delete window.drupalSettings.ajax[response.instanceId];
    }
  };

})(Drupal);
