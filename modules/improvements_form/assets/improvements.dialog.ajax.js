(function ($, Drupal) {

  /**
   * Override "closeDialog" ajax command.
   */
  var originalCloseDialogCommand = Drupal.AjaxCommands.prototype.closeDialog;
  Drupal.AjaxCommands.prototype.closeDialog = function (ajax, response, status) {
    // Prevent fatal error when dialog is already close
    var $dialog = $(response.selector);
    if ($dialog.length && $dialog.dialog('instance')) {
      // Call original command
      originalCloseDialogCommand.apply(this, arguments);
    }
  };

})(jQuery, Drupal);
