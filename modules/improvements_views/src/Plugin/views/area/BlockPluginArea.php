<?php

namespace Drupal\improvements_views\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\druhels\BlockHelper;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * @ViewsArea("block_plugin_area")
 */
class BlockPluginArea extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    $options['block_plugin_id'] = ['default' => ''];
    $options['block_title'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['block_plugin_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block plugin id'),
      '#default_value' => $this->options['block_plugin_id'],
    ];

    $form['block_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block title'),
      '#default_value' => $this->options['block_title'],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function render($empty = FALSE): array {
    return BlockHelper::viewByBlockPluginIdUsingNativeBuilder($this->options['block_plugin_id'], TRUE, $this->options['block_title']);
  }

}
