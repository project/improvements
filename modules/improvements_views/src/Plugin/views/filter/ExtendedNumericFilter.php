<?php

namespace Drupal\improvements_views\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\NumericFilter;

/**
 * @see improvements_views_plugins_filter_alter()
 */
class ExtendedNumericFilter extends NumericFilter {

  /**
   * {@inheritDoc}
   */
  public function operators(): array {
    $operators = parent::operators();

    $operators['in'] = [
      'title' => $this->t('Is one of'),
      'short' => $this->t('in'),
      'short_single' => $this->t('='),
      'method' => 'opIn',
      'values' => 1,
    ];

    return $operators;
  }

  /**
   * "in" operator.
   */
  protected function opIn(string $field): void {
    $values = explode(',', $this->value['value']);
    $this->query->addWhere($this->options['group'], $field, $values, $this->operator);
  }

}
