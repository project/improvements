<?php

namespace Drupal\improvements_views\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\Date;

/**
 * @see improvements_views_plugins_filter_alter()
 */
class ExtendedDateFilter extends Date {

  /**
   * {@inheritDoc}
   */
  public function acceptExposedInput($input): bool {
    $result = parent::acceptExposedInput($input);

    if (!$result && ($this->operator == 'between' || $this->operator == 'not between')) {
      if ($this->value['min'] && !$this->value['max']) {
        $result = TRUE;
        $this->value['max'] = '3000-01-01';
      }
      if (!$this->value['min'] && $this->value['max']) {
        $result = TRUE;
        $this->value['min'] = '1000-01-01';
      }
    }

    return $result;
  }

}
