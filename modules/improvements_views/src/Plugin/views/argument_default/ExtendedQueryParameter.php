<?php

namespace Drupal\improvements_views\Plugin\views\argument_default;

use Drupal\Component\Utility\NestedArray;
use Drupal\views\Plugin\views\argument_default\QueryParameter;

class ExtendedQueryParameter extends QueryParameter {

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    if (str_contains($this->options['query_param'], '[')) {
      $request = $this->view->getRequest();
      $options_query_param_array = explode('[', str_replace(']', '', $this->options['query_param']));
      $request_query_params = $request->query->all();

      if (NestedArray::keyExists($request_query_params, $options_query_param_array)) {
        $param = NestedArray::getValue($request_query_params, $options_query_param_array);
        if (is_array($param)) {
          $conjunction = ($this->options['multiple'] == 'and') ? ',' : '+';
          $param = implode($conjunction, $param);
        }
        return $param;
      }
      else {
        return $this->options['fallback'];
      }
    }
    else {
      return parent::getArgument();
    }
  }

}
