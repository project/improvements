<?php

namespace Drupal\improvements_views\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * @ViewsSort("greater_than")
 */
class GreaterThan extends SortPluginBase {

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    $this->ensureMyTable();
    $query = $this->query; /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $alias = $this->realField . '_greater_than';
    $query->addOrderBy(NULL, "IF({$this->tableAlias}.{$this->realField} > {$this->configuration['number']}, 1, 0)", $this->options['order'], $alias);
  }

}
