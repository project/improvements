<?php

namespace Drupal\improvements_views\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * @ViewsSort("is_null")
 */
// @TODO Add test
class IsNull extends SortPluginBase {

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    $this->ensureMyTable();
    $query = $this->query; /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $sort_alias = $this->tableAlias . '_' . $this->field;

    if (!empty($this->configuration['zero_is_null'])) {
      $query->addOrderBy(NULL, "($this->tableAlias.$this->realField IS NULL OR $this->tableAlias.$this->realField = 0)", $this->options['order'], $sort_alias);
    }
    else {
      $query->addOrderBy(NULL, "$this->tableAlias.$this->realField IS NULL", $this->options['order'], $sort_alias);
    }
  }

}
