<?php

namespace Drupal\improvements_views\Plugin\views\sort;

use Drupal\Core\Cache\Cache;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * @ViewsSort("is_current_user")
 */
class IsCurrentUser extends SortPluginBase {

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    if ($current_user_id = \Drupal::currentUser()->id()) {
      $this->ensureMyTable();
      $query = $this->query; /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $sort_alias = $this->realField . '_is_current_user';
      $query->addOrderBy(NULL, "IF ({$this->tableAlias}.{$this->realField} = $current_user_id, 1, 0)", $this->options['order'], $sort_alias);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
  }

}
