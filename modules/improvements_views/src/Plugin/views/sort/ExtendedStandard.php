<?php

namespace Drupal\improvements_views\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\sort\Standard;

// @TODO
class ExtendedStandard extends Standard {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    $options['null_position'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['null_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Null values position'),
      '#options' => [
        '' => $this->t('Default'),
        'after' => $this->t('After'),
        'before' => $this->t('Before'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // https://git.drupalcode.org/project/views_sort_null_field/-/blob/8.x-1.x/src/Plugin/views/sort/NullSort.php#L23

    parent::query();
  }

}
