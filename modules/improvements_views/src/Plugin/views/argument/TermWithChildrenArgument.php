<?php

namespace Drupal\improvements_views\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\druhels\TaxonomyHelper;
use Drupal\taxonomy\Entity\Term;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * @ViewsArgument("term_with_children")
 */
class TermWithChildrenArgument extends NumericArgument {

  /**
   * {@inheritDoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['break_phrase']['default'] = TRUE;
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $form['break_phrase']['#disabled'] = TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function query($group_by = FALSE): void {
    $term_id = $this->argument;
    if ($term_id && is_numeric($term_id) && ($term = Term::load($term_id))) {
      if ($all_children_terms_ids = TaxonomyHelper::getAllChildTermsIds($term->bundle(), $term_id)) {
        $this->argument .= '+' . implode('+', $all_children_terms_ids);
      }
    }

    parent::query($group_by);
  }

}
