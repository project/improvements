<?php

namespace Drupal\improvements_views\Plugin\views\field;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @ViewsField("map")
 */
class Map extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['path'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Array path'),
      '#description' => $this->t('Example "path.to.value" Equivalent to $array[\'path\'][\'to\'][\'value\'].'),
      '#required' => TRUE,
      '#default_value' => $this->options['path'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($value = $values->{$this->field_alias}) {
      $value = unserialize($value);
      $parents = explode('.', $this->options['path']);
      return NestedArray::getValue($value, $parents);
    }
    return NULL;
  }

}
