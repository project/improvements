<?php

namespace Drupal\improvements_views\Plugin\views\display_extender;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\druhels\ArrayHelper;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * @ViewsDisplayExtender(
 *   id = "improvements_display_extender",
 *   title = @Translation("Improvements display extender"),
 *   no_ui = FALSE
 * )
 */
class ImprovementsDisplayExtender extends DisplayExtenderPluginBase {

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') == 'css_class') {
      $form['css_class']['#title'] = $this->t('Views element CSS class name(s)');

      if ($this->displayHandler->getPluginId() == 'page') {
        $form['page_body_css_class'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Page body CSS class name(s)'),
          '#description' => $this->t('Separate multiple classes by spaces.') . ' ' . t('Supports tokens.'),
          '#default_value' => $this->options['page_body_css_class'] ?? '',
          '#weight' => -1,
        ];
      }

      $form['content_css_class'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Content CSS class name(s)'),
        '#description' => $this->t('Separate multiple classes by spaces.') . ' ' . t('Supports tokens.'),
        '#default_value' => $this->options['content_css_class'] ?? '',
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') == 'css_class') {
      $this->options['page_body_css_class'] = $form_state->getValue('page_body_css_class');
      $this->options['content_css_class'] = $form_state->getValue('content_css_class');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options): void {
    $options['css_class']['value'] = implode(' / ', ArrayHelper::removeEmptyElements([
      $this->options['page_body_css_class'] ?? '',
      $this->options['css_class'] ?? '',
      $this->options['content_css_class'] ?? '',
    ]));

    if (!$options['css_class']['value']) {
      $options['css_class']['value'] = $this->t('No');
    }
  }

}
