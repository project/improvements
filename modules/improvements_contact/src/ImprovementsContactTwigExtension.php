<?php

namespace Drupal\improvements_contact;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extensions.
 */
class ImprovementsContactTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('drupal_contact', 'improvements_contact_get_message_form'), // @TODO Add test
    ];
  }

}
