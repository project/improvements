<?php

namespace Drupal\improvements_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "machine_name",
 *   label = @Translation("Machine name"),
 *   description = @Translation("Allows to set paragraph machine name."),
 *   weight = 0,
 * )
 */
class MachineNameParagraphBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): array {
    $form['machine_name'] = [
      '#type' => 'textfield',
      '#title' => t('Machine name'),
      '#default_value' => $paragraph->get('machine_name')->value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): void {
    $filtered_values = $this->filterBehaviorFormSubmitValues($paragraph, $form, $form_state);
    $paragraph->set('machine_name', $filtered_values['machine_name'] ?? NULL);
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode): void {
    if ($machine_name = $paragraph->getBehaviorSetting($this->getPluginId(), 'value')) {
      //$build['#attributes']['class'] = Html::getClass($machine_name . '-paragraph');
    }
  }

}
