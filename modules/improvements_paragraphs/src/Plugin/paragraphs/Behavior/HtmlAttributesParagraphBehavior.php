<?php

namespace Drupal\improvements_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\druhels\ArrayHelper;
use Drupal\improvements\ImprovementsHelper;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "html_attributes",
 *   label = @Translation("Html attributes"),
 *   description = @Translation("Allows to set paragraph html attributes."),
 *   weight = 1,
 * )
 */
class HtmlAttributesParagraphBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): array {
    $attributes = $paragraph->getBehaviorSetting($this->getPluginId(), 'value', []);

    $form['#weight'] = 1;

    $form['html_attributes'] = [
      '#type' => 'textarea',
      '#title' => t('Html attributes'),
      '#description' => t('Example') . ' <code>class: my-paragraph-class</code>',
      '#default_value' => ArrayHelper::formatArrayAsKeyValueList($attributes),
      '#rows' => 2,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): void {
    $filtered_values = $this->filterBehaviorFormSubmitValues($paragraph, $form, $form_state);
    $attributes = !empty($filtered_values['html_attributes']) ? ArrayHelper::formatKeyValueListAsArray($filtered_values['html_attributes'], ': ') : [];
    $paragraph->setBehaviorSettings($this->getPluginId(), ['value' => $attributes]);
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode): void {
    if ($attributes = $paragraph->getBehaviorSetting($this->getPluginId(), 'value', [])) {
      $build['#attributes'] = array_merge($build['#attributes'] ?? [], ImprovementsHelper::formatArrayAsAttributes($attributes));
    }
  }

}
