<?php

namespace Drupal\improvements_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\druhels\ArrayHelper;
use Drupal\improvements\ImprovementsHelper;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "paragraph_html_attributes_behavior",
 *   label = @Translation("Html attributes (deprecated)"),
 *   description = @Translation("Allows to set paragraph html attributes."),
 *   weight = 0,
 * )
 */
class ParagraphAttributesBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): array {
    $attributes = $paragraph->getBehaviorSetting($this->getPluginId(), 'attributes', []);

    $form['attributes'] = [
      '#type' => 'textarea',
      '#title' => t('Html attributes (deprecated)'),
      '#description' => t('Example') . ' <code>class: my-paragraph-class</code>',
      '#default_value' => ArrayHelper::formatArrayAsKeyValueList($attributes),
      '#rows' => 2,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state): void {
    $filtered_values = $this->filterBehaviorFormSubmitValues($paragraph, $form, $form_state);
    $filtered_values['attributes'] = !empty($filtered_values['attributes']) ? ArrayHelper::formatKeyValueListAsArray($filtered_values['attributes'], ': ') : [];
    $paragraph->setBehaviorSettings($this->getPluginId(), $filtered_values);
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode): void {
    if ($behavior_attributes = $paragraph->getBehaviorSetting($this->getPluginId(), 'attributes', [])) {
      $build['#attributes'] = array_merge($build['#attributes'] ?? [], ImprovementsHelper::formatArrayAsAttributes($behavior_attributes));
    }
  }

}
