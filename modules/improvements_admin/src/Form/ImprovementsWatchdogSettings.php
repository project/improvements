<?php

namespace Drupal\improvements_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ImprovementsWatchdogSettings extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['improvements_admin.watchdog_settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'improvements_watchdog_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $watchdog_settings_config = $this->config('improvements_admin.watchdog_settings');

    $form['send_records_to_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send errors to e-mail'),
      '#default_value' => $watchdog_settings_config->get('send_records_to_email'),
    ];

    $form['send_records_email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#desription' => $this->t('Supports tokens'),
      '#default_value' => $watchdog_settings_config->get('send_records_email'),
      '#states' => [
        'visible' => [
          '#edit-send-records-to-email' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('improvements_admin.watchdog_settings')
      ->set('send_records_to_email', $form_state->getValue('send_records_to_email'))
      ->set('send_records_email', $form_state->getValue('send_records_email'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
