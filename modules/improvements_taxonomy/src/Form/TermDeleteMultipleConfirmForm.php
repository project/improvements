<?php

namespace Drupal\improvements_taxonomy\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm;
use Drupal\Core\Url;

/**
 * Provides a terms deletion confirmation form.
 */
class TermDeleteMultipleConfirmForm extends DeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.taxonomy_vocabulary.collection');
  }

}
