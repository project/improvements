<?php

namespace Drupal\improvements_taxonomy\Plugin\Menu\LocalTask;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class TermsBulkEditLocalTask extends LocalTaskDefault {

  /**
   * {@inheritDoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match): array {
    return [
      'taxonomy_vocabulary' => $route_match->getRawParameter('taxonomy_vocabulary'),
    ];
  }

}
