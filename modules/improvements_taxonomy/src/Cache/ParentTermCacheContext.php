<?php

namespace Drupal\improvements_taxonomy\Cache;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\druhels\TaxonomyHelper;

/**
 * Cache context ID: 'parent_term' (to vary by direct parents terms).
 */
class ParentTermCacheContext implements CacheContextInterface {

  protected RouteMatchInterface $routeMatch;

  /**
   * Class constructor.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritDoc}
   */
  public static function getLabel(): MarkupInterface {
    return t('Parent term');
  }

  /**
   * {@inheritDoc}
   */
  public function getContext(): string {
    if ($current_term_id = $this->routeMatch->getRawParameter('taxonomy_term')) {
      $parent_terms_id = TaxonomyHelper::getDirectParentsIds($current_term_id);
      return $parent_terms_id ? implode(',', $parent_terms_id) : (string)$current_term_id;
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return new CacheableMetadata();
  }

}
