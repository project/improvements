<?php

namespace Drupal\improvements_menu;

use Drupal\Core\Security\TrustedCallbackInterface;

class ImprovementsMenuTrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'menuLinkContentFormPreRender',
    ];
  }

  /**
   * Pre render callback for menu_link_content_form form.
   *
   * @see improvements_form_menu_link_content_form_alter()
   */
  public static function menuLinkContentFormPreRender(array $form): array {
    $form['description']['#weight'] = -1.1;
    return $form;
  }

}
