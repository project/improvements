(function (Drupal) {

  /**
   * Beautifier browser url.
   */
  Drupal.beautifierBrowserUrl = function () {
    if (window.location.href.indexOf('%')) {
      window.history.replaceState(null, null, decodeURI(window.location.pathname) + decodeURI(window.location.search) + window.location.hash);
    }
  };

  /**
   * Replace string multiple.
   */
  Drupal.stringReplaceMultiple = function (string, values) {
    for (var searchValue in values) {
      if (values.hasOwnProperty(searchValue)) {
        string = string.replace(new RegExp(searchValue, 'g'), values[searchValue]);
      }
    }
    return string;
  };

  /**
   * Preload images.
   */
  Drupal.preloadImages = function (urls) {
    var image;
    for (var i = 0; i < urls.length; i++) {
      if (urls[i]) {
        image = new Image();
        image.src = urls[i];
      }
    }
  };

  /**
   * Preload element images.
   */
  Drupal.preloadElementImages = function (element) {
    var imagesUrls = [];

    element.querySelectorAll('img').forEach(function (imgElement) {
      imagesUrls.push(imgElement.src);
    });

    Drupal.preloadImages(imagesUrls);
  };

  /**
   * Return elements attribute value.
   */
  Drupal.getElementsAttributeValue = function (elements, attributeName) {
    var values = [];

    // Not use Array.forEach() because "elements" maybe contains jQuery collection
    for (var i = 0; i < elements.length; i++) {
      var attributeValue = elements[i].getAttribute(attributeName);
      if (attributeValue !== '') {
        values.push(attributeValue);
      }
    }

    return values;
  };

  /**
   * Return dialog content wrapper.
   */
  Drupal.getDrupalModalElement = function () {
    var dialogElement = document.querySelector('#drupal-modal');

    if (!dialogElement) {
      dialogElement = document.createElement('div');
      dialogElement.id = 'drupal-modal';
      dialogElement.className = 'ui-front';

      document.body.append(dialogElement);
    }

    return dialogElement;
  }

  /**
   * Show content in modal dialog.
   */
  Drupal.showContentInDialog = function (content, dialogOptions) {
    var dialogElement = Drupal.getDrupalModalElement();
    dialogElement.innerHTML = '';
    dialogElement.append(content);
    Drupal.dialog(dialogElement, dialogOptions).showModal();
  }

  /**
   * Prevent default.
   */
  Drupal.preventDefault = function (element, eventName) {
    element.addEventListener(eventName, function (event) {
      event.preventDefault();
    });
  }

  /**
   * Return object from cookie.
   */
  Drupal.getObjectFromCookie = function (objectName) {
    var object = Cookies.get(objectName);
    object = object ? JSON.parse(object) : {};
    return typeof(object) === 'object' ? object : {};
  }

  /**
   * Save object in cookie.
   */
  Drupal.saveObjectInCookie = function (objectName, object, expires = 90) {
    Cookies.set(objectName, JSON.stringify(object), {
      expires: expires,
    });
  }

  /**
   * Update object properties in cookie.
   */
  Drupal.updateObjectInCookie = function (objectName, newProperties, expires) {
    var object = Drupal.getObjectFromCookie(objectName);
    object = Object.assign(object, newProperties);
    Drupal.saveObjectInCookie(objectName, object, expires);
  }

  /**
   * Return viewport height.
   */
  Drupal.getViewportHeight = function () {
    return window.innerHeight;
  }

  /**
   * Override function.
   */
  Drupal.functionOverride = function (object, functionName, options) {
    var originalFunction = object[functionName];

    object[functionName] = function () {
      if (options.before) {
        options.before.apply(this, arguments);
      }

      originalFunction.apply(this, arguments);

      if (options.after) {
        options.after.apply(this, arguments);
      }
    }
  }

  /**
   * Throttle function.
   * Source - https://doka.guide/js/throttle/
   */
  Drupal.throttle = function(callback, timeout) {
    var timer = null;

    return function perform(...args) {
      if (timer) return;

      timer = setTimeout(() => {
        callback(...args);
        clearTimeout(timer);
        timer = null;
      }, timeout);
    }
  }

})(Drupal);
