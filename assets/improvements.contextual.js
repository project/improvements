(function ($) {

  // Add current query string to "destination" in contextual links.
  // Notice: "document.addEventListener('my-custom-event')" not work with "$(...).trigger('my-custom-event')" so we have to use jQuery.
  // @see initContextual()
  // @TODO Rewrite to vanilla js after contextual links will refuse to jQuery
  $(document).on('drupalContextualLinkAdded', function (event, data) {
    if (window.location.search) {
      data.$el.find('.contextual-links a').each(function () {
        var url = this.getAttribute('href');
        this.setAttribute('href', url + Drupal.encodePath(window.location.search));
      });
    }
  });

})(jQuery);
