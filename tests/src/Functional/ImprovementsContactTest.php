<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\block\Entity\Block;
use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\improvements\ImprovementsHelper;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;

class ImprovementsContactTest extends BrowserTestBase {

  use AssertMailTrait;
  use BlockCreationTrait;
  use ImprovementsTestTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'block_content',
    'contact',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test contact form improvements.
   */
  private function _testContactFormImprovements(): void {
    $contact_form = ContactForm::create([
      'id' => 'test_contact_form',
      'label' => 'Contact form for ' . __FUNCTION__,
      'recipients' => ['test@mail.ru'],
    ]);
    $contact_form->save();
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/contact/manage/' . $contact_form->id());
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', '#block-local-tasks-block a[href="/contact/' . $contact_form->id() . '"]');

    // Clean
    $this->deleteEntities($contact_form);
    $this->drupalLogout();
  }

  /**
   * Test contact form improvements.
   */
  private function _testContactFormTokenInRecipients(): void {
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/contact/add');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldExists('recipients_new');
    $web_assert->fieldNotExists('recipients');

    $this->submitForm([
      'label' => 'Contact form for ' . __FUNCTION__,
      'id' => 'test_contact_form',
      'recipients_new' => '[site:mail]',
    ], 'Save');
    $this->dontSeeErrorMessage();
    $this->drupalGet('/admin/structure/contact/manage/test_contact_form');
    $web_assert->fieldValueEquals('recipients_new', '[site:mail]');

    $this->drupalGet('/contact/test_contact_form');
    $this->dontSeeErrorMessage();
    $this->submitForm([
      'subject[0][value]' => 'Test subject',
      'message[0][value]' => 'Test message',
    ], 'Send message');
    $this->dontSeeErrorMessage();
    $this->assertMailString('to', 'simpletest@example.com', 1);

    // Clean
    ContactForm::load('test_contact_form')->delete();
    $this->drupalLogout();
  }

  /**
   * Test contact form without additional settings.
   */
  private function _testContactFormWithoutAdditionalSettings(): void {
    $this->drupalLoginAsRoot();

    $this->drupalGet('/admin/structure/contact/add');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();

    $contact_form_values = [
      'label' => 'Contact form for ' . __FUNCTION__,
      'id' => 'without_additional_settings',
      'recipients_new' => 'test@example.com',
      'message' => 'Form submitted',
    ];
    $this->drupalGet('/admin/structure/contact/add');
    $this->dontSeeErrorMessage();
    $this->submitForm($contact_form_values, 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('has been added');

    $this->drupalGet('/contact/' . $contact_form_values['id']);
    $this->dontSeeErrorMessage();
    // @TODO Fix
    //$web_assert->elementTextContains('css', '#block-page-title-block', $contact_form_values['label']);
    $web_assert->elementTextContains('css', '#edit-name label', 'Your name');
    $web_assert->elementTextContains('css', '#edit-mail label', 'Your email address');
    $web_assert->elementTextContains('css', '#edit-subject-wrapper label', 'Subject');
    $web_assert->elementTextContains('css', '#edit-message-wrapper label', 'Message');
    $web_assert->elementAttributeExists('css', '#edit-message-0-value', 'required');
    $web_assert->elementExists('css', '#edit-preview');

    $this->submitForm([
      'name' => 'admin',
      'mail' => 'admin@example.com',
      'subject[0][value]' => 'Message subject',
      'message[0][value]' => 'Message body',
    ], 'Send message');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($contact_form_values['message']);

    // Clean
    $this->deleteEntities(ContactForm::load($contact_form_values['id']));
    $this->drupalLogout();
  }

  /**
   * Test contact form with advanced settings.
   */
  private function _testContactFormWithAdditionalSettings(): void {
    $this->drupalLoginAsRoot();

    $this->drupalGet('/admin/structure/contact/add');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', '#edit-additional-settings');
    $web_assert->elementExists('css', '#edit-page-title');

    $contact_form_values = [
      'label' => 'Contact form for ' . __FUNCTION__,
      'id' => 'with_advanced_settings',
      'recipients_new' => 'test@example.com',
      'message' => 'Form submitted',
      'page_title' => 'New contact form page title',
      'text_before' => 'Text before form',
      'text_after' => 'Text after form',
      'name_label' => 'New name label',
      'mail_label' => 'New e-mail label',
      'subject_label' => 'New subject label',
      'message_label' => 'New message label',
      'submit_button_text' => 'New submit button text',
      'name_placeholder' => 'Name placeholder',
      'email_placeholder' => 'E-mail placeholder',
      'email_subject' => 'New e-mail subject',
      'required_fields[name]' => FALSE,
      'required_fields[mail]' => FALSE,
      'required_fields[subject]' => FALSE,
      'required_fields[message]' => FALSE,
      'hide_preview_button' => TRUE,
    ];

    $second_contact_form_values = [
      'label' => 'Second contact form for ' . __FUNCTION__,
      'id' => 'second_with_advanced_settings',
      'recipients_new' => 'second@example.com',
      'message' => 'Second form submitted',
      'page_title' => 'Second contact form page title',
      'name_label' => 'Second name label',
      'mail_label' => 'Second e-mail label',
      'subject_label' => 'Second subject label',
      'message_label' => 'Second message label',
      'submit_button_text' => 'Second submit button text',
      'email_subject' => 'Second e-mail subject',
      'required_fields[name]' => FALSE,
      'required_fields[mail]' => TRUE,
      'required_fields[subject]' => FALSE,
      'required_fields[message]' => TRUE,
      'hide_preview_button' => TRUE,
    ];

    // Create first contact form
    $this->drupalGet('/admin/structure/contact/add');
    $this->dontSeeErrorMessage();
    $this->submitForm($contact_form_values, 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('has been added');

    // Create second contact form
    $this->drupalGet('/admin/structure/contact/add');
    $this->submitForm($second_contact_form_values, 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('has been added');

    // Test first contact form
    $this->drupalGet('/contact/' . $contact_form_values['id']);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($contact_form_values['text_before']);
    $web_assert->pageTextContains($contact_form_values['text_after']);
    $web_assert->elementTextContains('css', '#edit-name label', $contact_form_values['name_label']);
    $web_assert->elementTextContains('css', '#edit-mail label', $contact_form_values['mail_label']);
    $web_assert->elementTextContains('css', '#edit-subject-wrapper label', $contact_form_values['subject_label']);
    $web_assert->elementTextContains('css', '#edit-message-wrapper label', $contact_form_values['message_label']);
    // @TODO Fix
    //$web_assert->elementAttributeContains('css', '#edit-name', 'placeholder', $contact_form_values['name_placeholder']);
    //$web_assert->elementAttributeContains('css', '#edit-mail', 'placeholder', $contact_form_values['email_placeholder']);
    $web_assert->elementNotExists('css', '#edit-name.required');
    $web_assert->elementNotExists('css', '#edit-mail.required');
    $web_assert->elementNotExists('css', '#edit-subject-0-value.required');
    $web_assert->elementNotExists('css', '#edit-message-0-value.required');
    $web_assert->elementNotExists('css', '#edit-preview');
    $web_assert->elementAttributeContains('css', '#edit-submit', 'value', $contact_form_values['submit_button_text']);

    $this->submitForm([
      'name' => 'Dries',
      'mail' => 'dries@example.com',
      'subject[0][value]' => 'Message subject',
      'message[0][value]' => 'Message body',
    ], $contact_form_values['submit_button_text']);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($contact_form_values['message']);
    $this->assertMailString('subject', $contact_form_values['email_subject'], 1);

    // Test second contact form
    $this->drupalGet('/contact/' . $second_contact_form_values['id']);
    $this->dontSeeErrorMessage();
    $web_assert->elementTextContains('css', '#edit-name label', $second_contact_form_values['name_label']);
    $web_assert->elementTextContains('css', '#edit-mail label', $second_contact_form_values['mail_label']);
    $web_assert->elementTextContains('css', '#edit-subject-wrapper label', $second_contact_form_values['subject_label']);
    $web_assert->elementTextContains('css', '#edit-message-wrapper label', $second_contact_form_values['message_label']);
    $web_assert->elementNotExists('css', '#edit-name.required');
    $web_assert->elementExists('css', '#edit-mail.required');
    $web_assert->elementNotExists('css', '#edit-subject-0-value.required');
    $web_assert->elementExists('css', '#edit-message-0-value.required');
    $web_assert->elementNotExists('css', '#edit-preview');
    $web_assert->elementAttributeContains('css', '#edit-submit', 'value', $second_contact_form_values['submit_button_text']);

    $this->submitForm([
      'name' => 'Second Dries',
      'mail' => 'second-dries@example.com',
      'subject[0][value]' => 'Second message subject',
      'message[0][value]' => 'Second message body',
    ], $second_contact_form_values['submit_button_text']);
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($second_contact_form_values['message']);
    $this->assertMailString('subject', $second_contact_form_values['email_subject'], 1);

    // Clean
    $this->deleteEntities(ContactForm::load($contact_form_values['id']), ContactForm::load($second_contact_form_values['id']));
    $this->drupalLogout();
  }

  /**
   * Test render contact form.
   *
   * @covers improvements_contact_get_message_form
   */
  private function _testContactFormRender(): void {
    $contact_form_entity = ContactForm::create([
      'id' => 'test_render_contact_form',
      'label' => 'Test render contact form',
      'recipients' => ['test@example.com'],
    ]);
    $contact_form_entity->save();

    $this->drupalLoginAsRoot();
    $this->drupalGet('/improvements-test/contact-form/' . $contact_form_entity->id());
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldExists('name');
    $web_assert->fieldExists('mail');
    $web_assert->fieldExists('message[0][value]');

    // Clean
    $this->deleteEntities($contact_form_entity);
    $this->drupalLogout();
  }

  /**
   * Test contact form block.
   *
   * @covers \Drupal\improvements\Plugin\Block\ContactFormBlock
   */
  private function _testContactFormBlock(): void {
    $contact_form_entity = ContactForm::create([
      'id' => 'test_contact_form_block',
      'label' => 'Contact form for ' . __FUNCTION__,
      'recipients' => ['test@example.com'],
      'message' => 'Form submitted',
    ]);
    $contact_form_entity->save();

    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/block/library/' . $this->defaultTheme);
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('Contact form');

    $this->drupalGet('/admin/structure/block/add/contact_form_block/' . $this->defaultTheme);
    $this->dontSeeErrorMessage();
    $web_assert->fieldExists('settings[contact_form_id]');

    $this->submitForm([
      'id' => 'test_contact_form_block',
      'settings[contact_form_id]' => 'test_contact_form_block',
      'region' => 'content',
    ], 'Save block');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('The block configuration has been saved.');

    $this->drupalGetFrontPage();
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#block-test-contact-form-block');
    $web_assert->elementExists('css', '.contact-message-form');

    $this->submitForm([
      'name' => 'Dries',
      'mail' => 'test@example.com',
      'subject[0][value]' => 'Test subject',
      'message[0][value]' => 'Test message',
    ], 'Send message');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Form submitted');

    // Clean
    $this->deleteEntities($contact_form_entity, Block::load('test_contact_form_block'));
    $this->drupalLogout();
  }

  /**
   * Test contact form breadcrumb.
   */
  private function _testContactFormBreadcrumb(): void {
    $contact_form_entity = ContactForm::create([
      'id' => 'test_breadrumb',
      'label' => 'Contact form for ' . __FUNCTION__,
      'recipients' => ['test@example.com'],
    ]);
    $contact_form_entity->save();

    $this->drupalLoginAsRoot();
    $this->drupalGet('/contact/' . $contact_form_entity->id());
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementsCount('css', '#block-system-breadcrumb-block li', 1);

    // Clean
    $this->deleteEntities($contact_form_entity);
    $this->drupalLogout();
  }

}
