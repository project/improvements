<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\comment\Tests\CommentTestTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\token\TokenInterface;
use Drupal\user\Entity\Role;

class ImprovementsCommentTest extends BrowserTestBase {

  use BlockCreationTrait;
  use CommentTestTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'block_content',
    'comment',
    'datetime',
    'field_ui',
    'file',
    'filter',
    'image',
    'menu_ui',
    'node',
    'token',
    'path',
    'pathauto',
    'user',
  ];

  protected RendererInterface $renderer;

  protected ConfigFactoryInterface $configFactory;

  protected TokenInterface $tokenService;

  protected NodeTypeInterface $nodeType;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->renderer = $this->container->get('renderer');
    $this->configFactory = $this->container->get('config.factory');
    $this->tokenService = $this->container->get('token');

    // Create "page" node type
    $this->nodeType = $this->createContentType(['type' => 'page', 'name' => 'Page']);
    $this->addDefaultCommentField('node', 'page');

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);

    // Setting-up user permissions
    $anonymous_role = Role::load(Role::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access comments');
    $anonymous_role->save();
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test remove commented entity on comment form.
   *
   * @covers ::improvements_page_comment_reply_result_alter
   */
  private function _testRemoveCommentedEntityOnCommentForm(): void {
    $node = $this->createNode(['title' => 'Node for test ' . __FUNCTION__]);
    $this->drupalLoginAsRoot();
    $this->drupalGet('/comment/reply/node/' . $node->id() . '/comment');
    $this->dontSeeErrorMessage();

    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', '.comment-comment-form');
    $web_assert->elementNotExists('css', 'article');

    // Clean
    $node->delete();
    $this->drupalLogout();
  }

  /**
   * Test comment display fields "uid" and "created".
   *
   * @covers ::improvements_entity_base_field_info_alter
   */
  private function _testCommentShowAuthorAndDateFields(): void {
    $this->setFieldFormatterSettings('comment', 'comment', 'uid', ['type' => 'author']);
    $this->setFieldFormatterSettings('comment', 'comment', 'created', ['type' => 'timestamp']);

    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'body' => 'Test node content',
    ]);
    $comment = $this->createComment($node);

    $this->drupalLoginAsRoot();
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();

    $web_assert = $this->assertSession();
    $web_assert->elementTextContains('css', '#comment-' . $comment->id(), 'User ID');
    $web_assert->elementTextContains('css', '#comment-' . $comment->id(), 'Creation date');

    // Clean
    $node->delete();
    $this->drupalLogout();
  }

}
