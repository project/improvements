<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

class ImprovementsFieldTest extends BrowserTestBase {

  use BlockCreationTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'breakpoint',
    'datetime',
    'field_ui',
    'filter',
    'image',
    'node',
    'token',
  ];

  protected NodeTypeInterface $nodeType;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create "page" node type
    $this->nodeType = $this->createContentType(['type' => 'page', 'name' => 'Page']);

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);

    // Create filter formats
    $this->createFilterFormat(['format' => 'full_html', 'name' => 'Full HTML']);
    $this->createFilterFormat(['format' => 'raw_html', 'name' => 'Raw HTML']);
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test ComputedMapFieldItemList class.
   *
   * @covers \Drupal\improvements\Plugin\Field\ComputedMapFieldItemList
   */
  private function _testFieldTypeComputedMap(): void {
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'test_map_field' => [
        'first' => 'First',
        'second' => [
          'foo' => 'Foo',
          'bar' => 'Bar',
        ],
      ],
    ]);

    $this->assertSame('Foo', $node->get('test_computed_map_field_single')->value);
    $this->assertSame('Foo-Bar', $node->get('test_computed_map_field_multiple')->value);
    $this->assertSame('Foo', $node->get('test_computed_map_field_single')->first()->value);
    $this->assertSame('Foo-Bar', $node->get('test_computed_map_field_multiple')->first()->value);
    $this->assertSame([0 => ['value' => 'Foo']], $node->get('test_computed_map_field_single')->getValue());
    $this->assertSame([0 => ['value' => 'Foo-Bar']], $node->get('test_computed_map_field_multiple')->getValue());
    $this->assertSame(['value' => 'Foo'], $node->get('test_computed_map_field_single')->first()->getValue());
    $this->assertSame(['value' => 'Foo-Bar'], $node->get('test_computed_map_field_multiple')->first()->getValue());

    // Clean
    $node->delete();
  }

  /**
   * Test ThirdPartyDataItem.
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldType\ThirdPartyDataItem
   */
  private function _testFieldTypeThirdPartyData(): void {
    $node = $this->createNode(['title' => 'Node for test ' . __FUNCTION__]);
    $node->get('test_third_party_data')->setThirdPartyData('improvements_test', 'foo', 'bar');
    $node->get('test_third_party_data')->setThirdPartyData('improvements_test', 'baz', 'qux');
    $node->save();
    $node = Node::load($node->id());

    $this->assertSame('bar', $node->get('test_third_party_data')->getThirdPartyData('improvements_test', 'foo'));
    $this->assertSame('qux', $node->get('test_third_party_data')->getThirdPartyData('improvements_test', 'baz'));
    $this->assertSame(NULL, $node->get('test_third_party_data')->getThirdPartyData('improvements_test', 'bat'));

    // Clean
    $node->delete();
  }

  /**
   * Test textarea widget improvements.
   */
  private function _testFieldWidgetTextareaImprovements(): void {
    $web_assert = $this->assertSession();
    $this->drupalLoginAsRoot();
    $this->createField('node', 'page', 'field_text', 'text_long', 'Text', [], [], 'text_textarea');

    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'select[name="field_text[0][format]"]');

    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->dontSeeErrorMessage();
    $this->openEntityDisplayComponentSettings('field_text');
    $web_assert->fieldExists('fields[field_text][settings_edit_form][third_party_settings][improvements][max_length]');
    $web_assert->fieldExists('fields[field_text][settings_edit_form][third_party_settings][improvements][dynamic_height]');
    $web_assert->fieldExists('fields[field_text][settings_edit_form][third_party_settings][improvements][disable_editor]');
    $web_assert->fieldExists('fields[field_text][settings_edit_form][third_party_settings][improvements][hide_format_select]');

    $this->submitForm([
      'fields[field_text][settings_edit_form][third_party_settings][improvements][max_length]' => 10,
      'fields[field_text][settings_edit_form][third_party_settings][improvements][dynamic_height]' => TRUE,
      'fields[field_text][settings_edit_form][third_party_settings][improvements][disable_editor]' => TRUE,
      'fields[field_text][settings_edit_form][third_party_settings][improvements][hide_format_select]' => TRUE,
    ], 'Save');
    $this->dontSeeErrorMessage();

    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementAttributeContains('css', '#edit-field-text-0-value', 'maxlength', '10');
    $web_assert->elementNotExists('css', 'select[name="field_text[0][format]"]');

    // @TODO Add test disabled CKEditor

    // Clean
    $this->deleteField('field_text');
    $this->drupalLogout();
  }

  /**
   * Test textfield widget improvements.
   */
  private function _testFieldWidgetTextfieldImprovements(): void {
    $this->createField('node', 'page', 'field_string', 'string', 'String', [], [], 'string_textfield');
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->dontSeeErrorMessage();
    $this->openEntityDisplayComponentSettings('field_string');
    $web_assert->fieldExists('fields[field_string][settings_edit_form][third_party_settings][improvements][max_length]');

    $this->submitForm([
      'fields[field_string][settings_edit_form][third_party_settings][improvements][max_length]' => 10,
    ], 'Save');
    $this->dontSeeErrorMessage();

    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementAttributeContains('css', '#edit-field-string-0-value', 'maxlength', '10');

    // Clean
    $this->deleteField('field_string');
    $this->drupalLogout();
  }

  /**
   * Test string_textarea widget with "string" field type.
   */
  private function _testFieldWidgetStringTextareaWithStringFieldType(): void {
    $this->createField('node', 'page', 'field_string', 'string', 'String', [], [], 'string_textarea', [], 'string');
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->dontSeeErrorMessage();
    $web_assert->fieldValueEquals('fields[field_string][type]', 'string_textarea');

    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'textarea[name="field_string[0][value]"]');

    $this->submitForm([
      'title[0][value]' => 'Node for ' . __FUNCTION__,
      'field_string[0][value]' => 'Textarea value',
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Textarea value');

    // Clean
    $this->deleteEntities($this->getLastAddedNode());
    $this->deleteField('field_string');
    $this->drupalLogout();
  }

  /**
   * Test "file" widget improvements.
   */
  private function _testFieldWidgetFileImprovements(): void {
    $this->createField('node', 'page', 'field_file', 'file', 'File', [], [], 'file_generic');
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#edit-field-file-0-upload');

    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->dontSeeErrorMessage();
    $this->openEntityDisplayComponentSettings('field_file');
    $web_assert->fieldExists('fields[field_file][settings_edit_form][third_party_settings][improvements][autoupload]');
    $web_assert->fieldExists('fields[field_file][settings_edit_form][third_party_settings][improvements][upload_button_state]');

    $this->submitForm([
      'fields[field_file][settings_edit_form][third_party_settings][improvements][autoupload]' => FALSE,
    ], 'Save');
    $this->dontSeeErrorMessage();

    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#edit-field-file-0-upload.js-form-file--no-autoupload');
    $web_assert->elementExists('css', '#edit-field-file-0-upload-button.js-hide');

    $field_file_widget_settings['third_party_settings']['improvements']['upload_button_state'] = 1;
    $this->setFieldWidgetSettings('node', 'page', 'field_file', $field_file_widget_settings);
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#edit-field-file-0-upload-button');
    $web_assert->elementNotExists('css', '#edit-field-file-0-upload-button.js-hide');

    $field_file_widget_settings['third_party_settings']['improvements']['upload_button_state'] = 0;
    $this->setFieldWidgetSettings('node', 'page', 'field_file', $field_file_widget_settings);
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementNotExists('css', '#edit-field-file-0-upload-button');

    // Clean
    $this->deleteField('field_file');
    $this->drupalLogout();
  }

  /**
   * Test "date" widget functionality.
   */
  private function _testFieldWidgetDateImprovements(): void {
    $this->createField('node', 'page', 'field_date', 'datetime', 'Date', [], [], 'datetime_default');
    $this->drupalLoginAsRoot();
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();

    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', '.field--name-field-date');
    $web_assert->elementExists('css', '#edit-field-date-0-value-date');
    $web_assert->elementNotExists('css', '.field--name-field-date fieldset');

    // Clean
    $this->deleteField('field_date');
    $this->drupalLogout();
  }

  /**
   * Test field widget "multiple_string_textarea".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldWidget\MultipleStringTextarea
   */
  private function _testFieldWidgetMultipleStringTextarea(): void {
    $this->createField('node', 'page', 'field_strings', 'string', 'Strings', ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], [], 'multiple_string_textarea');

    $this->drupalLoginAsRoot();
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', 'textarea[name="field_strings[value]"]');

    $this->submitForm([
      'title[0][value]' => 'Node for test ' . __FUNCTION__,
      'field_strings[value]' => "foo\nbar\nbaz",
    ], 'Save');
    $this->dontSeeErrorMessage();
    $node_id = $this->getLastAddedNodeId();
    $node = Node::load($node_id);
    $this->assertSame([
      0 => ['value' => 'foo'],
      1 => ['value' => 'bar'],
      2 => ['value' => 'baz'],
    ], $node->get('field_strings')->getValue());

    // Clean
    $this->deleteEntities($node);
    $this->deleteField('field_strings');
    $this->drupalLogout();
  }

  /**
   * Test field widget "multiple_string_textfield".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldWidget\MultipleStringTextfield
   */
  private function _testFieldWidgetMultipleStringTextfield(): void {
    $this->createField('node', 'page', 'field_strings', 'string', 'Strings', ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], [], 'multiple_string_textfield');

    $this->drupalLoginAsRoot();
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', 'input[name="field_strings[value]"][type="text"]');

    $this->submitForm([
      'title[0][value]' => 'Node for test ' . __FUNCTION__,
      'field_strings[value]' => "foo, bar, baz",
    ], 'Save');
    $this->dontSeeErrorMessage();
    $node_id = $this->getLastAddedNodeId();
    $node = Node::load($node_id);
    $this->assertSame([
      0 => ['value' => 'foo'],
      1 => ['value' => 'bar'],
      2 => ['value' => 'baz'],
    ], $node->get('field_strings')->getValue());

    // Clean
    $this->deleteEntities($node);
    $this->deleteField('field_strings');
    $this->drupalLogout();
  }

  /**
   * Test "entity_reference_views_formatter" formatter.
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\EntityReferenceViewsFormatter
   */
  private function _testFieldFormatterEntityReferenceViews(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();
    $field_name = 'field_views_formatter_test';
    $views_name = 'test_entity_reference_views_formatter';

    $this->createField(
      entity_type: 'node',
      entity_bundle: 'page',
      field_name: $field_name,
      field_type: 'entity_reference',
      field_label: 'Entity reference',
      storage_settings: ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED],
      formatter_type: 'entity_reference_views_formatter',
      formatter_options: ['weight' => 0],
    );

    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'option[value="entity_reference_views_formatter"][selected]');

    $this->openEntityDisplayComponentSettings($field_name);
    $web_assert->elementExists('css', 'select[name="fields[' . $field_name . '][settings_edit_form][settings][views]"]');
    $web_assert->elementExists('css', 'option[value="' . $views_name . ':default"]');

    $this->submitForm([
      'fields[' . $field_name . '][settings_edit_form][settings][views]' => $views_name . ':default',
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Views: ' . $views_name . ':default');

    $node1 = $this->createNode(['title' => 'Node 1 for test ' . __FUNCTION__]);
    $node2 = $this->createNode(['title' => 'Node 2 for test ' . __FUNCTION__]);
    $node3 = $this->createNode(['title' => 'Node 3 for test ' . __FUNCTION__]);
    $node4 = $this->createNode([
      'title' => 'Node 4 for test ' . __FUNCTION__,
      $field_name => [$node1->id(), $node2->id()],
    ]);

    $this->drupalGetEntityPage($node4);
    $web_assert->elementExists('css', '.views-element-container');
    $web_assert->pageTextContains($node1->label());
    $web_assert->pageTextContains($node2->label());
    $web_assert->pageTextNotContains($node3->label());

    $this->drupalGetEntityPage($node3);
    $web_assert->elementNotExists('css', '.views-element-container');

    // Clean
    $this->deleteEntities($node1, $node2, $node3, $node4);
    $this->deleteField($field_name);
    $this->drupalLogout();
  }

  /**
   * Test ExtendedImageFormatter
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\ExtendedImageFormatter
   */
  private function _testFieldFormatterExtendedImage(): void {
    $web_assert = $this->assertSession();

    $this->createField(
      entity_type: 'node',
      entity_bundle: 'page',
      field_name: 'field_image',
      field_type: 'image',
      field_label: 'Image',
      storage_settings: ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED],
      widget_type: 'image_image',
      formatter_type: 'extended_image_formatter',
    );
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $this->openEntityDisplayComponentSettings('field_image');
    $web_assert->elementTextContains('css', '.plugin-name', 'Extended image formatter');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][image_style]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][first_image_style]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][image_link]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][image_link_style]');

    $this->submitForm([
      'fields[field_image][settings_edit_form][settings][image_style]' => 'thumbnail',
      'fields[field_image][settings_edit_form][settings][first_image_style]' => 'large',
      'fields[field_image][settings_edit_form][settings][image_link]' => 'file',
      'fields[field_image][settings_edit_form][settings][image_link_style]' => 'medium',
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Image style: thumbnail');

    $first_image = $this->createFileEntity(['uri' => 'public://first-image.png']);
    $second_image = $this->createFileEntity(['uri' => 'public://second-image.png']);
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'field_image' => [
        $first_image,
        $second_image
      ],
    ]);
    $this->drupalGetEntityPage($node);
    $web_assert->responseContains('/styles/large/public/');
    $web_assert->responseContains('/styles/thumbnail/public/');

    // Clean
    $this->deleteEntities($first_image, $second_image, $node);
    $this->deleteField('field_image');
    $this->drupalLogout();
  }

  /**
   * Test formatter "image_with_mobile_alternative".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\ImageWithMobileAlternativeFormatter
   */
  private function _testFieldFormatterImageWithMobileAlternative(): void {
    $web_assert = $this->assertSession();

    // Preparing environment
    $this->drupalLoginAsRoot();
    $this->createField(
      entity_type: 'node',
      entity_bundle: 'page',
      field_name: 'field_image',
      field_type: 'image',
      field_label: 'Image',
      widget_type: 'image_image',
      formatter_type: 'image_with_mobile_alternative',
      formatter_options: [
      'settings' => [
        'breakpoint_group' => 'improvements_test',
      ]
    ]);
    $this->createField('node', 'page', 'field_mobile_image', 'image', 'Mobile image');

    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $this->openEntityDisplayComponentSettings('field_image');
    $web_assert->elementTextContains('css', '.plugin-name', 'Image with mobile alternative');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][image_style]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][breakpoint_group]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][mobile_breakpoint]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][mobile_field]');
    $web_assert->fieldExists('fields[field_image][settings_edit_form][settings][mobile_image_style]');

    $this->submitForm([
      'fields[field_image][settings_edit_form][settings][mobile_breakpoint]' => 'improvements_test.mobile',
      'fields[field_image][settings_edit_form][settings][mobile_field]' => 'field_mobile_image',
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Breakpoint group: improvements_test');
    $web_assert->pageTextContains('Mobile breakpoint: improvements_test.mobile');
    $web_assert->pageTextContains('Mobile field: field_mobile_image');

    $first_image = $this->createFileEntity(['uri' => 'public://first-image.png']);
    $second_image = $this->createFileEntity(['uri' => 'public://second-image.png']);
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'field_image' => $first_image,
      'field_mobile_image' => $second_image,
    ]);
    $this->drupalGetEntityPage($node);
    $web_assert->elementExists('css', 'picture');
    $web_assert->elementExists('css', 'source[srcset="' . $second_image->createFileUrl() . '"][type="image/png"][media="(max-width: 640px)"]');
    $web_assert->elementExists('css', 'img[src="' . $first_image->createFileUrl() . '"]');

    // Clean
    $this->deleteEntities($first_image, $second_image, $node);
    $this->deleteField('field_image');
    $this->deleteField('field_mobile_image');
    $this->drupalLogout();
  }

  /**
   * Test formatter "human_readable_decimal_formatter".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\HumanReadableDecimalFormatter
   */
  private function _testFieldFormatterHumanReadableDecimal(): void {
    $field_decimal_storage_settings = [
      'settings' => [
        'precision' => 10,
        'scale' => 2,
      ]
    ];
    $this->createField(
      entity_type: 'node',
      entity_bundle: 'page',
      field_name: 'field_decimal',
      field_type: 'decimal',
      field_label: 'Decimal',
      storage_settings: $field_decimal_storage_settings,
      formatter_type: 'human_readable_decimal_formatter',
    );

    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', 'select[name="fields[field_decimal][type]"] option[value="human_readable_decimal_formatter"][selected]');

    $this->openEntityDisplayComponentSettings('field_decimal');
    $web_assert->elementTextContains('css', '.plugin-name', 'Human-readable decimal formatter');
    $web_assert->fieldExists('fields[field_decimal][settings_edit_form][settings][thousand_separator]');
    $web_assert->fieldExists('fields[field_decimal][settings_edit_form][settings][prefix_suffix]');

    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'field_decimal' => 0.10,
    ]);
    $this->drupalGetEntityPage($node);
    $this->savePageContent();
    $web_assert->elementTextContains('css', 'article', '0.1');

    // Clean
    $this->deleteEntities($node);
    $this->deleteField('field_decimal');
  }

  /**
   * Test field formatter "string_list_formatter".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\StringListFormatter
   */
  private function _testFieldFormatterStringList(): void {
    $this->createField('node', 'page', 'field_strings', 'string', 'Strings', ['cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED], [], NULL, [], 'string_list_formatter');
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldValueEquals('fields[field_strings][type]', 'string_list_formatter');

    $this->openEntityDisplayComponentSettings('field_strings');
    $web_assert->fieldExists('fields[field_strings][settings_edit_form][settings][list_class]');

    $list_class = 'list-test-class';
    $this->submitForm([
      'fields[field_strings][settings_edit_form][settings][list_class]' => $list_class,
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('List css class: ' . $list_class);

    $node = $this->createNode([
      'title' => 'Node for ' . __FUNCTION__,
      'field_strings' => [
        'foo',
        'bar',
      ],
    ]);
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->elementTextContains('css', 'ul.' . $list_class . ' li:nth-child(1)', 'foo');
    $web_assert->elementTextContains('css', 'ul.' . $list_class . ' li:nth-child(2)', 'bar');

    // Clean
    $this->deleteEntities($node);
    $this->deleteField('field_strings');
    $this->drupalLogout();
  }

  /**
   * Test field formatter "text_rows_list_formatter".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\TextRowsListFormatter
   */
  private function _testFieldFormatterTextRowsList(): void {
    $this->createField('node', 'page', 'field_text', 'string_long', 'Text', [], [], NULL, [], 'text_rows_list_formatter');
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldValueEquals('fields[field_text][type]', 'text_rows_list_formatter');

    $this->openEntityDisplayComponentSettings('field_text');
    $web_assert->fieldExists('fields[field_text][settings_edit_form][settings][list_class]');

    $list_class = 'list-test-class';
    $this->submitForm([
      'fields[field_text][settings_edit_form][settings][list_class]' => $list_class,
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('List css class: ' . $list_class);

    $node = $this->createNode([
      'title' => 'Node for ' . __FUNCTION__,
      'field_text' => "foo\nbar\nbaz",
    ]);
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->elementTextContains('css', 'ul.' . $list_class . ' li:nth-child(1)', 'foo');
    $web_assert->elementTextContains('css', 'ul.' . $list_class . ' li:nth-child(2)', 'bar');

    // Clean
    $this->deleteEntities($node);
    $this->deleteField('field_text');
    $this->drupalLogout();
  }

  /**
   * Test field formatter "text_without_escaping".
   *
   * @covers \Drupal\improvements\Plugin\Field\FieldFormatter\TextWithoutEscapingFormatter
   */
  private function _testFieldFormatterTextWithoutEscaping(): void {
    $this->createField('node', 'page', 'field_string', 'string', 'String', [], [], NULL, [], 'text_without_escaping');
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', 'select[name="fields[field_string][type]"] option[value="text_without_escaping"][selected]');

    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'field_string' => 'foo <b class="test-bar-class">bar</b> baz',
    ]);
    $this->drupalGetEntityPage($node);
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '.test-bar-class');

    // Clean
    $this->deleteEntities($node);
    $this->deleteField('field_string');
    $this->drupalLogout();
  }

  /**
   * Open field widget/formatter settings.
   */
  private function openEntityDisplayComponentSettings(string $field_name) {
    $this->submitForm([], $field_name . '_settings_edit');
    $this->dontSeeErrorMessage();
  }

}
