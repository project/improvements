<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

class ImprovementsMenuTest extends BrowserTestBase {

  use BlockCreationTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'block_content',
    'menu_link_content',
    'menu_ui',
    'node',
  ];

  protected NodeTypeInterface $nodeType;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create "page" node type
    $this->nodeType = $this->createContentType(['type' => 'page', 'name' => 'Page']);

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test menu improvements.
   */
  private function _testMenuImprovements(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    // Test autofocus in menu form
    $this->drupalGet('/admin/structure/menu/add');
    $this->dontSeeErrorMessage();
    $web_assert->elementAttributeExists('css', '#edit-label', 'autofocus');

    // Test menu link form
    $this->drupalGet('/admin/structure/menu/manage/main/add');
    $this->dontSeeErrorMessage();
    $web_assert->elementAttributeExists('css', '#edit-title-0-value', 'autofocus');
    $web_assert->elementExists('css', '#edit-link-0-uri');
    $web_assert->elementNotExists('css', '#edit-link-0-uri[required]');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test "Add child item" in menu.
   */
  private function _testMenuLinkAddChildItem(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $menu_link = MenuLinkContent::create([
      'title' => 'Home',
      'link' => ['uri' => 'internal:/'],
      'menu_name' => 'main',
    ]);
    $menu_link->save();
    $this->drupalGet('/admin/structure/menu/manage/main');
    $this->dontSeeErrorMessage();
    $web_assert->elementTextContains('css', 'ul[data-drupal-selector="edit-links-menu-plugin-idmenu-link-content' . $menu_link->uuid() . '-operations"]', 'Add child item');
    $web_assert->elementAttributeContains('css', 'ul[data-drupal-selector="edit-links-menu-plugin-idmenu-link-content' . $menu_link->uuid() . '-operations"] li:last-child a', 'href', '/admin/structure/menu/manage/main/add?parent=main%3Amenu_link_content%3A' . $menu_link->uuid());

    $this->drupalGet('/admin/structure/menu/manage/main/add', ['query' => ['parent' => 'main:menu_link_content:' . $menu_link->uuid()]]);
    $this->dontSeeErrorMessage();
    $web_assert->elementAttributeExists('css', 'option[value="main:menu_link_content:' . $menu_link->uuid() . '"]', 'selected');

    // Clean
    $menu_link->delete();
    $this->drupalLogout();
  }

  /**
   * Test menu link advanced settings.
   */
  private function _testMenuLinkAdvancedSettings(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $this->drupalGet('/admin/structure/menu/manage/main/add');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#edit-open-in-dialog');
    $web_assert->elementExists('css', '#edit-target-blank');
    $web_assert->elementExists('css', '#edit-item-class');
    $web_assert->elementExists('css', '#edit-link-class');

    $this->submitForm([
      'title[0][value]' => 'Test link',
      'link[0][uri]' => '/user',
      'open_in_dialog' => TRUE,
      'hide_href' => TRUE,
      'target_blank' => TRUE,
      'item_class' => 'test-link-item',
      'link_class' => 'test-link',
    ], 'Save');
    $this->dontSeeErrorMessage();
    $this->drupalGetFrontPage();
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#block-main-menu-block li.test-link-item');
    $web_assert->elementExists('css', '#block-main-menu-block a.test-link');
    $web_assert->elementExists('css', '#block-main-menu-block a.test-link.use-ajax');
    $web_assert->elementAttributeContains('css', '#block-main-menu-block a.test-link', 'data-dialog-type', 'modal');
    $web_assert->elementAttributeContains('css', '#block-main-menu-block a.test-link', 'target', '_blank');
    $web_assert->elementAttributeContains('css', '#block-main-menu-block a.test-link', 'href', '#');
    $web_assert->elementAttributeContains('css', '#block-main-menu-block a.test-link', 'data-ajax-url', '/user');
    $web_assert->elementExists('css', 'script[src^="/core/misc/ajax.js"]');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test menu block tweak cache.
   */
  private function _testMenuBlockTweakCache(): void {
    $web_assert = $this->assertSession();
    $this->drupalLoginAsRoot();

    $node1 = $this->createNode(['title' => 'Node 1 for ' . __FUNCTION__]);
    $node2 = $this->createNode(['title' => 'Node 2 for ' . __FUNCTION__]);
    $menu = $this->createMenu('test_menu', 'Menu for ' . __FUNCTION__);
    $menu_link1 = $this->createMenuLinkForEntity($node1, $menu->id());
    $menu_link2 = $this->createMenuLinkForEntity($node2, $menu->id());

    $this->drupalGet('/admin/structure/menu/manage/' . $menu->id());
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($menu_link1->label());
    $web_assert->pageTextContains($menu_link2->label());

    $block = $this->drupalPlaceBlock('system_menu_block:' . $menu->id(), [
      'id' => 'test_menu_block',
      'region' => 'content',
      'weight' => 0,
      'label' => 'Block for ' . __FUNCTION__,
    ]);
    $block_html_id = '#block-test-menu-block';

    $this->drupalGetEntityPage($node1);
    $web_assert->elementExists('css', $block_html_id);
    $this->assertEquals(1, $this->countRenderCache('entity_view:block:' . $block->id()));

    $this->drupalGetEntityPage($node2);
    $web_assert->elementExists('css', $block_html_id);
    $this->assertEquals(2, $this->countRenderCache('entity_view:block:' . $block->id()));

    $this->drupalGet('/admin/structure/block/manage/' . $block->id());
    $this->dontSeeErrorMessage();
    $web_assert->fieldExists('tweak_cache');
    $web_assert->checkboxNotChecked('tweak_cache');
    $this->clearRenderCache();
    $this->submitForm([
      'tweak_cache' => 1,
    ], 'Save block');
    $this->dontSeeErrorMessage();
    $this->drupalGet('/admin/structure/block/manage/' . $block->id());
    $web_assert->checkboxChecked('tweak_cache');

    $this->drupalGetEntityPage($node1);
    $web_assert->elementExists('css', $block_html_id);
    $this->assertEquals(1, $this->countRenderCache('entity_view:block:' . $block->id()));

    $this->drupalGetEntityPage($node2);
    $web_assert->elementExists('css', $block_html_id);
    $this->assertEquals(1, $this->countRenderCache('entity_view:block:' . $block->id()));

    // Clean
    $this->deleteEntities($block, $node1, $node2, $menu);
  }

  /**
   * Test menu block items limitation.
   */
  private function _testMenuBlockItemsLimitation(): void {
    $web_assert = $this->assertSession();
    $this->drupalLoginAsRoot();

    $node1 = $this->createNode(['title' => 'Node 1 for ' . __FUNCTION__]);
    $node2 = $this->createNode(['title' => 'Node 2 for ' . __FUNCTION__]);
    $menu = $this->createMenu('test_menu', 'Menu for ' . __FUNCTION__);
    $menu_link1 = $this->createMenuLinkForEntity($node1, $menu->id());
    $menu_link2 = $this->createMenuLinkForEntity($node2, $menu->id());

    $block = $this->drupalPlaceBlock('system_menu_block:' . $menu->id(), [
      'id' => 'test_menu_block',
      'region' => 'content',
      'weight' => 0,
      'label' => 'Block for ' . __FUNCTION__,
    ]);
    $block_html_id = '#block-test-menu-block';

    // Check menu block render
    $this->drupalGetFrontPage();
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', $block_html_id);
    $web_assert->elementTextContains('css', $block_html_id, $menu_link1->label());
    $web_assert->elementTextContains('css', $block_html_id, $menu_link2->label());

    // Check menu block form
    $this->drupalGet('/admin/structure/block/manage/' . $block->id());
    $this->dontSeeErrorMessage();
    $web_assert->fieldExists('items_limitation[method]');
    $web_assert->fieldExists('items_limitation[items][]');

    // Save menu block settings
    $this->submitForm([
      'items_limitation[method]' => 'exclude_selected',
      'items_limitation[items][]' => 'menu_link_content:' . $menu_link1->uuid(),
    ], 'Save block');
    $this->dontSeeErrorMessage();
    $this->drupalGet('/admin/structure/block/manage/' . $block->id());
    $web_assert->fieldValueEquals('items_limitation[method]', 'exclude_selected');
    $web_assert->elementExists('css', 'option[value="menu_link_content:' . $menu_link1->uuid() . '"][selected]');

    // Check menu block with new settings
    $this->drupalGetFrontPage();
    $web_assert->elementExists('css', $block_html_id);
    $web_assert->elementTextNotContains('css', $block_html_id, $menu_link1->label());
    $web_assert->elementTextContains('css', $block_html_id, $menu_link2->label());

    // Change menu block settings and check result
    $this->drupalGet('/admin/structure/block/manage/' . $block->id());
    $this->submitForm([
      'items_limitation[method]' => 'exclude_non_selected',
      'items_limitation[items][]' => 'menu_link_content:' . $menu_link1->uuid(),
    ], 'Save block');
    $this->dontSeeErrorMessage();
    $this->drupalGetFrontPage();
    $web_assert->elementExists('css', $block_html_id);
    $web_assert->elementTextContains('css', $block_html_id, $menu_link1->label());
    $web_assert->elementTextNotContains('css', $block_html_id, $menu_link2->label());

    // Clean
    $this->deleteEntities($block, $node1, $node2, $menu);
  }

}
