<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\token\TokenInterface;
use Drupal\views\ViewExecutable;

class ImprovementsViewsTest extends BrowserTestBase {

  use BlockCreationTrait;
  use ImprovementsTestTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'comment',
    'token',
    'views',
    'views_ui',
  ];

  protected RendererInterface $renderer;

  protected ConfigFactoryInterface $configFactory;

  protected TokenInterface $tokenService;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->renderer = $this->container->get('renderer');
    $this->configFactory = $this->container->get('config.factory');
    $this->tokenService = $this->container->get('token');

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);

    $this->configFactory->getEditable('views.settings')->set('display_extenders', ['improvements_display_extender'])->save();
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test Views field plugin "map".
   *
   * @covers \Drupal\improvements\Plugin\views\field\Map
   */
  private function _testViewsPluginFieldMap(): void {
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'test_map_field' => [
        'foo' => [
          'bar' => 'Hello world!',
        ],
      ],
    ]);
    $views_build = views_embed_view('test_views_map_field_plugin');
    $views_output = (string)$this->renderer->renderRoot($views_build);
    $this->assertStringContainsString('<span class="field-content">Hello world!</span>', $views_output);

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * Test Views extended numeric filter.
   *
   * @covers \Drupal\improvements\Plugin\views\filter\ExtendedNumericFilter
   */
  private function _testViewsPluginFilterExtendedNumeric(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $view = $this->createView([
      'id' => 'test_extended_numeric_filter',
      'label' => 'Test extended numeric filter',
      'base_table' => 'node_field_data',
    ]);
    $this->drupalGet('/admin/structure/views/nojs/add-handler/' . $view->id() . '/default/filter');
    $this->dontSeeErrorMessage();
    $this->submitForm(['name[node_field_data.nid]' => TRUE], 'Add and configure filter criteria');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'select[name="options[operator]"] option[value="in"]');

    $this->submitForm([
      'options[operator]' => 'in',
      'options[value][value]' => '1,2,3',
    ], 'Apply');
    $this->dontSeeErrorMessage();

    // @TODO Check generated sql query

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test Views "is_current_user" sort plugin.
   *
   * @covers \Drupal\improvements\Plugin\views\sort\IsCurrentUser
   */
  private function _testViewsPluginSortIsCurrentUserSort(): void {
    $this->drupalLoginAsRoot();
    $sql = (string)$this->getViewQuery('test_sort_by_current_user');
    if (version_compare(\Drupal::VERSION, '9.0.0', '<')) {
      $this->assertStringContainsString('IF (node_field_data.uid = 1, ' . $this->rootUser->id() . ', 0) AS uid_is_current_user', $sql);
      $this->assertStringContainsString('ORDER BY uid_is_current_user', $sql);
    }
    else {
      $this->assertStringContainsString('IF (node_field_data.uid = 1, ' . $this->rootUser->id() . ', 0) AS "uid_is_current_user"', $sql);
      $this->assertStringContainsString('ORDER BY "uid_is_current_user"', $sql);
    }

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test sort by comment author is current user
   */
  private function _testViewsPluginSortByCommentAuthorIsCurrentUser(): void {
    $this->drupalLoginAsRoot();
    $sql = (string)$this->getViewQuery('test_sort_by_comment_author_is_current_user');
    if (version_compare(\Drupal::VERSION, '9.0.0', '<')) {
      $this->assertStringContainsString('IF (comment_field_data.uid = 1, ' . $this->rootUser->id() . ', 0) AS uid_is_current_user', $sql);
      $this->assertStringContainsString('ORDER BY uid_is_current_user', $sql);
    }
    else {
      $this->assertStringContainsString('IF (comment_field_data.uid = 1, ' . $this->rootUser->id() . ', 0) AS "uid_is_current_user"', $sql);
      $this->assertStringContainsString('ORDER BY "uid_is_current_user"', $sql);
    }

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test Views "greater_than" sort plugin.
   *
   * @covers \Drupal\improvements\Plugin\views\sort\GreaterThan
   */
  private function _testViewsPluginSortGreaterThan(): void {
    $this->drupalLoginAsRoot();
    $sql = (string)$this->getViewQuery('test_sort_by_comment_author_is_registered');
    if (version_compare(\Drupal::VERSION, '9.0.0', '<')) {
      $this->assertStringContainsString('IF(comment_field_data.uid > 0, 1, 0) AS uid_greater_than', $sql);
      $this->assertStringContainsString('ORDER BY uid_greater_than', $sql);
    }
    else {
      $this->assertStringContainsString('IF(comment_field_data.uid > 0, 1, 0) AS "uid_greater_than"', $sql);
      $this->assertStringContainsString('ORDER BY "uid_greater_than"', $sql);
    }

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test token [view:total-rows-in-current-page].
   */
  private function _testViewsTotalRowsToken(): void {
    $this->deleteNodesByType('page');
    $node1 = $this->createNode(['title' => 'Node 1 for test ' . __FUNCTION__]);
    $node2 = $this->createNode(['title' => 'Node 2 for test ' . __FUNCTION__]);
    $node3 = $this->createNode(['title' => 'Node 3 for test ' . __FUNCTION__]);

    $view_executable = $this->getExecutedView('test_total_rows_token');
    $total_rows = $this->tokenService->replace('[view:total-rows-in-current-page]', ['view' => $view_executable]);
    $this->assertSame('3', $total_rows);

    $view_executable = $this->getExecutedView('test_total_rows_token', 'default', [], function (ViewExecutable $view) {
      $view->setItemsPerPage(2);
    });
    $total_rows = $this->tokenService->replace('[view:total-rows-in-current-page]', ['view' => $view_executable]);
    $this->assertSame('2', $total_rows);

    $view_build = views_embed_view('test_total_rows_token');
    $view_output = (string)$this->renderer->renderRoot($view_build);
    $this->assertStringContainsString('<div class="content-css-class total-rows-3">', $view_output);

    // Clean
    $this->deleteEntities($node1, $node2, $node3);
  }

  /**
   * Test prefix in Views display tab name.
   *
   * @covers ::improvements_views_ui_display_top_alter
   */
  private function _testViewsDisplayTabNamePrefix(): void {
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/views/view/test_total_rows_token/edit/default');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementTextContains('css', '#views-display-menu-tabs', '(default) Default');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test Views optional date exposed filter.
   */
  private function _testViewsOptionalDateExposedFilter(): void {
    $nodes = [
      1 => $this->createNode(['created' => strtotime('2020-06-01')]),
      2 => $this->createNode(['created' => strtotime('2021-08-01')]),
    ];
    $web_assert = $this->assertSession();

    $this->drupalGet('/test-optional-date-exposed-filter');
    $web_assert->pageTextContains($nodes[1]->label());
    $web_assert->pageTextContains($nodes[2]->label());

    $this->submitForm([
      'created[min]' => '2020-01-01',
      'created[max]' => '2022-01-01',
    ], 'Apply');
    $web_assert->pageTextContains($nodes[1]->label());
    $web_assert->pageTextContains($nodes[2]->label());

    $this->submitForm([
      'created[min]' => '2021-01-01',
      'created[max]' => '2022-01-01',
    ], 'Apply');
    $web_assert->pageTextContains($nodes[2]->label());
    $web_assert->pageTextNotContains($nodes[1]->label());

    $this->submitForm([
      'created[min]' => '2020-01-01',
      'created[max]' => '',
    ], 'Apply');
    $web_assert->pageTextContains($nodes[1]->label());
    $web_assert->pageTextContains($nodes[2]->label());

    $this->submitForm([
      'created[min]' => '2021-01-01',
      'created[max]' => '',
    ], 'Apply');
    $web_assert->pageTextContains($nodes[2]->label());
    $web_assert->pageTextNotContains($nodes[1]->label());

    $this->submitForm([
      'created[min]' => '',
      'created[max]' => '2021-01-01',
    ], 'Apply');
    $web_assert->pageTextContains($nodes[1]->label());
    $web_assert->pageTextNotContains($nodes[2]->label());
  }

}
