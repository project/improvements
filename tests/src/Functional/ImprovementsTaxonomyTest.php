<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Routing\RouteMatch;
use Drupal\druhels\TaxonomyHelper;
use Drupal\improvements\Cache\ParentTermCacheContext;
use Drupal\node\NodeTypeInterface;
use Drupal\pathauto\Entity\PathautoPattern;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\VocabularyInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\token\TokenInterface;
use Symfony\Component\Routing\Route;

class ImprovementsTaxonomyTest extends BrowserTestBase {

  use ImprovementsTestTrait;
  use NodeCreationTrait;
  use TaxonomyTestTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'field_ui',
    'node',
    'taxonomy',
    'path',
    'pathauto',
    'views',
    'views_ui',
  ];

  protected NodeTypeInterface $nodeType;

  protected VocabularyInterface $vocabulary;

  protected TokenInterface $tokenService;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->tokenService = $this->container->get('token');

    // Create "page" node type
    $this->nodeType = $this->createContentType(['type' => 'page', 'name' => 'Page']);

    // Create "category" vocabulary
    $this->vocabulary = Vocabulary::create(['name' => 'Categories', 'vid' => 'category']);
    $this->vocabulary->save();

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test condition plugin "term_depth".
   *
   * @covers \Drupal\improvements\Plugin\Condition\TermDepth
   */
  private function _testConditionTermDepth(): void {
    $condition_manager = $this->container->get('plugin.manager.condition'); /** @var ConditionManager $condition_manager */
    $term_depth_condition = $condition_manager->createInstance('term_depth'); /** @var ConditionInterface $term_depth_condition */
    $term1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1 for test ' . __FUNCTION__]);
    $term1 = Term::load($term1->id());
    $term2 = $this->createTerm($this->vocabulary, ['name' => 'Term 2 for test ' . __FUNCTION__, 'parent' => [$term1->id()]]);
    $term2 = Term::load($term2->id());

    $term_depth_condition->setContextValue('taxonomy_term', $term1);
    $term_depth_condition->setConfig('depth', []);
    $this->assertSame(TRUE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term1);
    $term_depth_condition->setConfig('depth', [0]);
    $this->assertSame(TRUE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term1);
    $term_depth_condition->setConfig('depth', [0, 1, 2]);
    $this->assertSame(TRUE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term1);
    $term_depth_condition->setConfig('depth', [1]);
    $this->assertSame(FALSE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term2);
    $term_depth_condition->setConfig('depth', [0]);
    $this->assertSame(FALSE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term2);
    $term_depth_condition->setConfig('depth', [1]);
    $this->assertSame(TRUE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term2);
    $term_depth_condition->setConfig('depth', [2]);
    $this->assertSame(FALSE, $term_depth_condition->evaluate());

    $term_depth_condition->setContextValue('taxonomy_term', $term2);
    $term_depth_condition->setConfig('depth', [0, 2]);
    $this->assertSame(FALSE, $term_depth_condition->evaluate());

    // Clean
    $this->deleteEntities($term1, $term2);
  }

  /**
   * Test Pathauto "term depth" condition.
   */
  private function _testPathautoTermDepthCondition(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $pathauto_pattern = PathautoPattern::create([
      'id' => 'term',
      'label' => 'Term',
      'type' => 'canonical_entities:taxonomy_term',
      'pattern' => 'term/[term:tid]',
    ]);
    $pathauto_pattern->save();

    $this->drupalGet('/admin/config/search/path/patterns/' . $pathauto_pattern->id());
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('Term depth condition');
    $web_assert->elementExists('css', 'input[name="depth_condition[depth][0]"]');
    $web_assert->elementExists('css', 'input[name="depth_condition[depth][1]"]');
    $web_assert->elementExists('css', 'input[name="depth_condition[depth][2]"]');

    $this->submitForm([
      'depth_condition[depth][0]' => TRUE,
    ], 'Save');
    $this->dontSeeErrorMessage();
    $this->drupalGet('/admin/config/search/path/patterns/' . $pathauto_pattern->id());
    $this->dontSeeErrorMessage();
    $web_assert->checkboxChecked('depth_condition[depth][0]');

    $term1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1 for test _testPathautoTermDepthCondition']);
    $term1 = Term::load($term1->id());
    $this->assertSame('/term/' . $term1->id(), $term1->get('path')->alias);

    $term2 = $this->createTerm($this->vocabulary, ['name' => 'Term 2 for test _testPathautoTermDepthCondition', 'parent' => $term1->id()]);
    $term2 = Term::load($term2->id());
    $this->assertSame(NULL, $term2->get('path')->alias);

    // Clean
    $this->deleteEntities($term1, $term2, $pathauto_pattern);
    $this->drupalLogout();
  }

  /**
   * Test "stored_depth" tem field.
   */
  private function _testTermFieldStoredDepth(): void {
    $term1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1 for test _testTermDepth']);
    $this->assertSame(0, $term1->get('stored_depth')->value);
    $this->assertSame(0, TaxonomyHelper::getTermDepth($term1));

    $term2 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 2 for test _testTermDepth',
      'parent' => [$term1->id()],
    ]);
    $this->assertSame(1, $term2->get('stored_depth')->value);
    $this->assertSame(1, TaxonomyHelper::getTermDepth($term2));

    $term3 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 3 for test _testTermDepth',
      'parent' => [$term2->id()],
    ]);
    $term4 = $this->createTerm($this->vocabulary, [
      'name' => 'Term 4 for test _testTermDepth',
      'parent' => [$term2->id()],
    ]);
    $this->assertSame(2, $term3->get('stored_depth')->value);
    $this->assertSame(2, $term4->get('stored_depth')->value);
    $this->assertSame(2, TaxonomyHelper::getTermDepth($term3));
    $this->assertSame(2, TaxonomyHelper::getTermDepth($term3));

    $term2->set('parent', [0]);
    $term2->save();
    $this->assertSame(0, $term2->get('stored_depth')->value);
    $this->assertSame(0, TaxonomyHelper::getTermDepth($term2));
    $term3 = Term::load($term3->id()); // Load actual term
    $term4 = Term::load($term4->id()); // Load actual term
    $this->assertSame(1, (int)$term3->get('stored_depth')->value);
    $this->assertSame(1, (int)$term4->get('stored_depth')->value);
    $this->assertSame(1, TaxonomyHelper::getTermDepth($term3));
    $this->assertSame(1, TaxonomyHelper::getTermDepth($term4));

    // Clean
    $this->deleteEntities($term1, $term2, $term3, $term4);
  }

  /**
   * Test "has_children" term field.
   */
  private function _testTermFieldHasChildren(): void {
    $term_1 = $this->createTerm($this->vocabulary);
    $this->assertFalse((bool)$term_1->get('has_children')->value);
    $this->assertFalse(TaxonomyHelper::termHasChilds($term_1->id()));

    $term_1_1 = $this->createTerm($this->vocabulary, ['parent' => $term_1->id()]);
    $this->assertFalse((bool)$term_1_1->get('has_children')->value);
    $this->assertFalse(TaxonomyHelper::termHasChilds($term_1_1->id()));
    $term_1 = Term::load($term_1->id()); // Load actual term
    $this->assertTrue((bool)$term_1->get('has_children')->value);
    $this->assertTrue(TaxonomyHelper::termHasChilds($term_1->id(), TRUE));

    $term_1_1->delete();
    $term_1 = Term::load($term_1->id()); // Load actual term
    $this->assertFalse((bool)$term_1->get('has_children')->value);
    $this->assertFalse(TaxonomyHelper::termHasChilds($term_1->id(), TRUE));

    // Clean
    $this->deleteEntities($term_1_1, $term_1);
  }

  /**
   * Test taxonomy improvements.
   */
  private function _testTaxonomyImprovements(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $this->drupalGet('/admin/structure/taxonomy/manage/' . $this->vocabulary->id() . '/add');
    $this->dontSeeErrorMessage();
    $web_assert->elementAttributeExists('css', 'input[name="name[0][value]"]', 'autofocus');

    $term = $this->createTerm($this->vocabulary, ['name' => 'Term for test _testTaxonomyImprovements']);
    $this->drupalGet('/admin/structure/taxonomy/manage/' . $this->vocabulary->id() . '/overview');
    $this->dontSeeErrorMessage();
    $web_assert->elementNotExists('css', '#edit-reset-alphabetical');

    $this->drupalGet('/admin/structure/taxonomy/manage/category/overview');
    $this->dontSeeErrorMessage();
    $web_assert->elementTextContains('css', '#block-local-tasks-block a[href="/admin/structure/taxonomy/manage/category/overview"]', 'Terms');
    $web_assert->elementTextContains('css', '#block-local-tasks-block a[href="/admin/structure/taxonomy/manage/category"]', 'Edit vocabulary');

    // Clean
    $this->deleteEntities($term);
    $this->drupalLogout();
  }

  /**
   * Test link "Add child" term.
   *
   * @covers ::improvements_entity_operation
   */
  private function _testTermAddChildLink(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    $term = $this->createTerm($this->vocabulary, ['name' => 'Term for test _testTermAddChildLink']);
    $this->drupalGet('/admin/structure/taxonomy/manage/' . $this->vocabulary->id() . '/overview');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'tr[data-drupal-selector="edit-terms-tid' . $term->id() . '0"]');

    $this->drupalGet('/admin/structure/taxonomy/manage/' . $this->vocabulary->id() . '/add', ['query' => ['parent' => $term->id()]]);
    $this->dontSeeErrorMessage();
    $web_assert->elementAttributeExists('css', 'select[name="parent[]"] option[value="' . $term->id() . '"]', 'selected');

    // Clean
    $this->deleteEntities($term);
    $this->drupalLogout();
  }

  /**
   * Test parent_term cache context.
   *
   * @covers \Drupal\improvements\Cache\ParentTermCacheContext
   */
  private function _testCacheContextParentTerm(): void {
    $term1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1 for test _testCacheContextParentTerm']);
    $term2 = $this->createTerm($this->vocabulary, ['name' => 'Term 2 for test _testCacheContextParentTerm', 'parent' => [$term1->id()]]);

    $route = new Route('/taxonomy/term/{taxonomy_term}');
    $route_match = new RouteMatch('entity.taxonomy_term.canonical', $route, ['taxonomy_term' => $term2], ['taxonomy_term' => $term2->id()]);
    $cache_context = new ParentTermCacheContext($route_match);
    $this->assertSame((string)$term1->id(), $cache_context->getContext());

    $term3 = $this->createTerm($this->vocabulary, ['name' => 'Term 3 for test _testCacheContextParentTerm']);
    $term2->set('parent', [$term1->id(), $term3->id()]);
    $term2->save();
    $this->assertSame($term1->id() . ',' . $term3->id(), $cache_context->getContext());

    $route = new Route('/');
    $route_match = new RouteMatch('<front>', $route);
    $cache_context = new ParentTermCacheContext($route_match);
    $this->assertSame('', $cache_context->getContext());

    // Clean
    $this->deleteEntities($term2, $term3, $term1);
  }

  /**
   * Test Views argument plugin "term_with_children".
   *
   * @covers \Drupal\improvements\Plugin\views\argument\TermWithChildrenArgument
   */
  private function _testViewsPluginArgumentTermWithChildren(): void {
    $this->drupalLoginAsRoot();

    $this->createField('node', 'page', 'field_category', 'entity_reference', 'Category', [
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);

    $this->drupalGet('/admin/structure/views/nojs/add-handler/test_argument_term_with_children/default/argument');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldExists('name[node__field_category.field_category_target_id_with_children]');

    $this->submitForm([
      'name[node__field_category.field_category_target_id_with_children]' => 'node__field_category.field_category_target_id_with_children',
    ], 'Add and configure contextual filters');
    $this->dontSeeErrorMessage();
    $this->drupalGet('/admin/structure/views/view/test_argument_term_with_children');
    $this->dontSeeErrorMessage();
    $this->submitForm([], 'Save');
    $this->dontSeeErrorMessage();

    $term_1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1']);
    $term_2 = $this->createTerm($this->vocabulary, ['name' => 'Term 2']);
    $term_1_1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1.1', 'parent' => [$term_1->id()]]);
    $term_1_2 = $this->createTerm($this->vocabulary, ['name' => 'Term 1.2', 'parent' => [$term_1->id()]]);

    $query = $this->getViewQuery('test_argument_term_with_children', 'default', [$term_1->id()]);
    $this->assertSame([
      (string)$term_1->id(),
      (string)$term_1_1->id(),
      (string)$term_1_2->id(),
    ], current($query->getArguments()));

    $query = $this->getViewQuery('test_argument_term_with_children', 'default', [$term_2->id()]);
    $this->assertSame((string)$term_2->id(), current($query->getArguments()));

    // Clean
    $this->deleteEntities($term_1, $term_2);
    $this->deleteField('field_category');
    $this->drupalLogout();
  }

  /**
   * Test breadcrumbs on edit term page.
   */
  private function _testTermEditPageBreadcrumbs(): void {
    $this->drupalLoginAsRoot();
    $term = $this->createTerm($this->vocabulary, ['name' => 'Term for test _testTermEditPageBreadcrumbs']);

    $this->drupalGet('/taxonomy/term/' . $term->id() . '/edit');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', '#block-system-breadcrumb-block a[href="/"]');
    $web_assert->elementExists('css', '#block-system-breadcrumb-block a[href="/admin"]');
    $web_assert->elementExists('css', '#block-system-breadcrumb-block a[href="/admin/structure"]');
    $web_assert->elementExists('css', '#block-system-breadcrumb-block a[href="/admin/structure/taxonomy"]');
    $web_assert->elementExists('css', '#block-system-breadcrumb-block a[href="/admin/structure/taxonomy/manage/' . $term->bundle() . '"]');
    $web_assert->elementExists('css', '#block-system-breadcrumb-block a[href="/admin/structure/taxonomy/manage/' . $term->bundle() . '/overview"]');

    // Clean
    $this->deleteEntities($term);
    $this->drupalLogout();
  }

  /**
   * Test bulk edit terms Views.
   */
  private function _testTermBulkEditViews(): void {
    $web_assert = $this->assertSession();
    $this->drupalLoginAsRoot();
    $tags_vocabulary = Vocabulary::create(['name' => 'Tags', 'vid' => 'tag']);
    $tags_vocabulary->save();
    $term1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1 for test ' . __FUNCTION__]);
    $term2 = $this->createTerm($tags_vocabulary, ['name' => 'Term 2 for test ' . __FUNCTION__]);

    $this->drupalGet('/admin/structure/taxonomy/manage/category/overview');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#block-local-tasks-block a[href="/admin/structure/taxonomy/manage/category/bulk-edit"]');

    $this->drupalGet('/admin/structure/taxonomy/manage/tag/overview');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#block-local-tasks-block a[href="/admin/structure/taxonomy/manage/tag/bulk-edit"]');

    $this->drupalGet('/admin/structure/taxonomy/manage/category/bulk-edit');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'form#views-form-admin-terms-page-category');
    $web_assert->pageTextContains($term1->label());
    $web_assert->pageTextNotContains($term2->label());

    $this->drupalGet('/admin/structure/taxonomy/manage/tag/bulk-edit');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'form#views-form-admin-terms-page-tag');
    $web_assert->pageTextContains($term2->label());
    $web_assert->pageTextNotContains($term1->label());

    // Clean
    $this->deleteEntities($term1, $term2, $tags_vocabulary);
    $this->drupalLogout();
  }

  /**
   * Test tokens [site:third_party_setting_name].
   */
  private function _testTokenTermParentsField(): void {
    $term_1     = $this->createTerm($this->vocabulary, ['name' => 'Term 1 for test _testTokenTermParentsField']);
    $term_1_1   = $this->createTerm($this->vocabulary, ['name' => 'Term 1.1 for test _testTokenTermParentsField', 'parent' => $term_1->id()]);
    $term_1_2   = $this->createTerm($this->vocabulary, ['name' => 'Term 1.2 for test _testTokenTermParentsField', 'parent' => $term_1->id()]);
    $term_1_1_1 = $this->createTerm($this->vocabulary, ['name' => 'Term 1.1.1 for test _testTokenTermParentsField', 'parent' => $term_1_1->id()]);

    $this->assertEquals("{$term_1->label()}, {$term_1_1->label()}", $this->tokenService->replace('[term:parents_field:name]', ['term' => $term_1_1_1]));
    $this->assertEquals("{$term_1->label()}/{$term_1_1->label()}", $this->tokenService->replace('[term:parents_field:name:join:/]', ['term' => $term_1_1_1]));
    $this->assertEquals("{$term_1->id()}, {$term_1_1->id()}", $this->tokenService->replace('[term:parents_field:tid]', ['term' => $term_1_1_1]));
    $this->assertEquals("{$term_1->id()}", $this->tokenService->replace('[term:parents_field:tid]', ['term' => $term_1_1]));
    $this->assertEquals('', $this->tokenService->replace('[term:parents_field:tid]', ['term' => $term_1]));
    $this->assertEquals("{$term_1->label()}/{$term_1_1->label()}/{$term_1_1_1->label()}", $this->tokenService->replace('[term:parents_field:name:join:/]/[term:name]', ['term' => $term_1_1_1]));

    $this->deleteEntities($term_1_1_1, $term_1_2, $term_1_1, $term_1);
  }

}
