<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\Core\Render\RendererInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

class ImprovementsThemingTest extends BrowserTestBase {

  use BlockCreationTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'node',
  ];

  protected RendererInterface $renderer;

  protected NodeTypeInterface $nodeType;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->renderer = $this->container->get('renderer');

    // Create "page" node type
    $this->nodeType = $this->createContentType(['type' => 'page', 'name' => 'Page']);
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test '#theme'=>'simple_field'.
   */
  private function _testThemeSimpleField(): void {
    $build = [
      '#theme' => 'simple_field',
      '#field_name' => 'custom_field',
      '#title' => 'Custom field',
      '#entity_type' => 'node',
      '#bundle' => 'article',
      '#items' => 'Field value',
    ];
    $html = (string)$this->renderer->renderRoot($build);
    $this->assertStringContainsString($build['#title'], $html);
    $this->assertStringContainsString($build['#items'], $html);
  }

  /**
   * Test '#theme'=>'picture'.
   */
  private function _testThemePicture(): void {
    $build = [
      '#theme' => 'picture',
      '#img' => [
        'src' => '/sites/default/files/logo.jpg',
        'alt' => '',
      ],
      '#source' => [
        'mobile' => [
          'srcset' => '/sites/default/files/logo-mobile.jpg',
          'media' => '(min-width: 600px)',
        ],
      ],
    ];
    $html = (string)$this->renderer->renderRoot($build);
    $this->assertStringContainsString('<picture>', $html);
    $this->assertStringContainsString('<source srcset="/sites/default/files/logo-mobile.jpg" media="(min-width: 600px)" />', $html);
    $this->assertStringContainsString('<img src="/sites/default/files/logo.jpg" alt="" />', $html);
  }

  /**
   * Test '#theme_wrappers'=>['noindex'].
   */
  private function _testThemeNoindex(): void {
    $build = [
      '#markup' => 'Markup',
      '#theme_wrappers' => ['noindex'],
    ];
    $html = (string)$this->renderer->renderRoot($build);
    $this->assertStringContainsString("<!--noindex-->\nMarkup\n<!--/noindex-->", $html);
  }

  /**
   * Test js theme path settings.
   */
  private function _testThemePathsInDrupalSettings(): void {
    $this->drupalGetFrontPage();
    $this->dontSeeErrorMessage();
    $drupal_settings = $this->getDrupalSettings();
    $this->assertSame('core/themes/' . $this->defaultTheme, $drupal_settings['path']['currentThemePath']);
    $this->assertSame('core/themes/' . $this->defaultTheme, $drupal_settings['path']['defaultThemePath']);
  }

  /**
   * Test remove "off canvas page wrapper" for anonymouse users.
   *
   * @covers \Drupal\improvements\ImprovementsTrustedCallbacks::pagePreRender
   */
  private function _testRemoveOffCanvasPageWrapper(): void {
    $this->drupalGetFrontPage();
    $this->dontSeeErrorMessage();
    $this->assertSession()->elementNotExists('css', '.dialog-off-canvas-main-canvas');

    $this->drupalLoginAsRoot();
    $this->drupalGetFrontPage();
    $this->dontSeeErrorMessage();
    $this->assertSession()->elementExists('css', '.dialog-off-canvas-main-canvas');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test twig function preg_matches().
   *
   * @covers \Drupal\improvements\ImprovementsTwigExtension::pregMatches
   */
  private function _testTwigFunctionPregMatch(): void {
    $build = [
      '#type' => 'inline_template',
      '#template' => "{{ preg_matches('/[0-9]+/', 'Drupal 9', 0) }}",
    ];
    $output = (string)$this->renderer->renderRoot($build);
    $this->assertSame('9', $output);

    $build = [
      '#type' => 'inline_template',
      '#template' => "{% if preg_matches('/Drupal [0-9]+/', 'Drupal 9') %}ok{% endif %}",
    ];
    $output = (string)$this->renderer->renderRoot($build);
    $this->assertSame('ok', $output);
  }

}
