<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;

class ImprovementsFormTest extends BrowserTestBase {

  use BlockCreationTrait;
  use ImprovementsTestTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'file',
    'jquery_ui_slider',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test "option" element attributes.
   */
  private function _testFormSelectOptionsAttributes(): void {
    $this->drupalGet('/improvements-test/test-form');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', 'select[name="select1"] option[value="foo"]');
    $web_assert->elementExists('css', 'select[name="select2"] option[value="foo"][data-custom-attribute="foo-custom-attribute"].foo-test-class');
    $web_assert->elementExists('css', 'select[name="select2"] option[value="bar"]:disabled');
    $web_assert->elementExists('css', 'select[name="select2"] option[value="baz"]');

    $this->submitForm([
      'select1' => 'bar',
      'select2' => 'baz',
    ], 'Submit');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('[select1] => bar');
    $web_assert->pageTextContains('[select2] => baz');
  }

  /**
   * Test "managed_file" element improvements.
   */
  private function _testFormManagedFileImprovements(): void {
    $this->drupalGet('/improvements-test/test-form');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementExists('css', '#edit-file-without-autoupload-upload');
    $web_assert->elementExists('css', '#edit-file-without-autoupload-upload.js-form-file--no-autoupload');
    $web_assert->elementExists('css', '#edit-file-without-autoupload-upload-button.js-hide');
    $web_assert->elementExists('css', '#edit-file-with-button-upload-button');
    $web_assert->elementNotExists('css', '#edit-file-with-button-upload-button.js-hide');
    $web_assert->elementNotExists('css', '#edit-file-without-button-upload-button');
  }

  /**
   * Test '#title_display'=>'placeholder'.
   */
  private function _testFormInputTitleDisplayPlaceholder(): void {
    $this->drupalGet('/improvements-test/test-form');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->elementAttributeContains('css', '#edit-label-in-placeholder', 'placeholder', 'Label in placeholder');

    $this->submitForm([
      'label_in_placeholder' => 'foo',
    ], 'Submit');
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains('[label_in_placeholder] => foo');
  }

  /**
   * Test '#type'=>'range_slider'.
   *
   * @covers \Drupal\improvements\Element\RangeSlider
   */
  private function _testFormElementRangeSlider(): void {
    $this->drupalGet('/improvements-test/test-form');
    $this->dontSeeErrorMessage();
    $this->submitForm([
      'range_slider[min]' => 5,
      'range_slider[max]' => 60,
    ], 'Submit');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('[min] => 5');
    $web_assert->pageTextContains('[max] => 60');
  }

  /**
   * Test tracking form submit functionality.
   */
  private function _testFormTrackingSubmit(): void {
    $this->drupalGet('/improvements-test/test-form');
    $this->dontSeeErrorMessage();
    $this->submitForm([], 'Submit');
    $this->dontSeeErrorMessage();
    $drupal_settings = $this->getDrupalSettings();
    $this->assertSame('improvements_test_form', $drupal_settings['submittedForms'][0]);
  }

}
