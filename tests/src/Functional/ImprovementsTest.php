<?php

namespace Drupal\Tests\improvements\Functional;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Url;
use Drupal\improvements\Ajax\AddClassCommand;
use Drupal\improvements\Ajax\ConsoleLogCommand;
use Drupal\improvements\Ajax\FormResetCommand;
use Drupal\improvements\Ajax\HistoryReplaceStateCommand;
use Drupal\improvements\Ajax\PageReloadCommand;
use Drupal\improvements\Ajax\RemoveClassCommand;
use Drupal\improvements\Cache\CookiesHashCacheContext;
use Drupal\improvements\Cache\EntityFieldCacheContext;
use Drupal\improvements\Cache\EntityTypeCacheContext;
use Drupal\improvements\Cache\IsEntityPageCacheContext;
use Drupal\improvements_test\Entity\SecondConfigEntity;
use Drupal\improvements_test\Entity\TestConfigEntity;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\improvements\Traits\ImprovementsTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\token\TokenInterface;
use Drupal\user\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Route;

class ImprovementsTest extends BrowserTestBase {

  use BlockCreationTrait;
  use ImprovementsTestTrait;
  use NodeCreationTrait;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'improvements',
    'improvements_test',
    'block',
    'block_content',
    'contact',
    'datetime',
    'field_ui',
    'file',
    'filter',
    'image',
    'menu_ui',
    'node',
    'token',
    'path',
    'pathauto',
    'user',
  ];

  protected RendererInterface $renderer;

  protected ConfigFactoryInterface $configFactory;

  protected TokenInterface $tokenService;

  protected NodeTypeInterface $nodeType;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->renderer = $this->container->get('renderer');
    $this->configFactory = $this->container->get('config.factory');
    $this->tokenService = $this->container->get('token');

    // Create "page" node type
    $this->nodeType = $this->createContentType(['type' => 'page', 'name' => 'Page']);

    // Create content block type
    $this->createBlockContentType();

    // Place blocks
    $this->placeBlock('system_menu_block:main', [
      'id' => 'main_menu_block',
      'region' => 'help',
      'weight' => 0,
      'label' => 'Main menu block',
    ]);
    $this->placeBlock('system_breadcrumb_block', [
      'id' => 'system_breadcrumb_block',
      'region' => 'help',
      'weight' => 1,
      'label' => 'Breadcrumb block',
    ]);
    $this->placeBlock('page_title_block', [
      'id' => 'page_title_block',
      'region' => 'help',
      'weight' => 2,
    ]);
    $this->placeBlock('local_tasks_block', [
      'id' => 'local_tasks_block',
      'region' => 'help',
      'weight' => 3,
    ]);
    $this->placeBlock('local_actions_block', [
      'id' => 'local_actions_block',
      'region' => 'help',
      'weight' => 4,
    ]);

    // Create filter formats
    $this->createFilterFormat(['format' => 'full_html', 'name' => 'Full HTML']);
    $this->createFilterFormat(['format' => 'raw_html', 'name' => 'Raw HTML']);

    // Setting-up user permissions
    $anonymous_role = Role::load(Role::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access site-wide contact form');
    $anonymous_role->save();
  }

  /**
   * Main test function.
   */
  public function testMain() {
    $this->runAllPrivateTests();
  }

  /**
   * Test altering page title.
   *
   * @covers \Drupal\improvements\ImprovementsTrustedCallbacks
   */
  private function _testPageTitleAlter(): void {
    $this->drupalGet('/improvements-test/altered-route-title');
    $this->dontSeeErrorMessage();
    $this->assertSession()->elementTextContains('css', 'h1', 'Altered route title');
  }

  /**
   * Test create link with attribute href="#".
   *
   * @covers \Drupal\improvements\ImprovementsTrustedCallbacks::linkPostRender
   */
  private function _testLinkWithOnlySharpSymbol(): void {
    $build = [
      '#type' => 'link',
      '#url' => Url::fromRoute('<none>', [], ['fragment' => '<none>']),
      '#title' => 'Test',
    ];
    $html = (string)$this->renderer->renderRoot($build);
    $this->assertSame('<a href="#">' . $build['#title'] . '</a>', $html);
  }

  /**
   * Test modules list filter autofocus.
   *
   * @covers ::improvements_form_system_modules_alter
   */
  private function _testModulesListAutofocus(): void {
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/modules');
    $this->dontSeeErrorMessage();
    $this->assertSession()->elementExists('css', '#edit-text[autofocus]');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test link for edit block content.
   *
   * @covers ::improvements_form_block_admin_display_form_alter
   */
  private function _testContentBlockEditLink(): void {
    $block_content = $this->createBlockContent();
    $block = $this->placeBlockContent($block_content, ['id' => 'test_edit_content_link']);
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/structure/block');
    $this->dontSeeErrorMessage();
    $this->assertSession()->elementExists('css', 'tr[data-drupal-selector="edit-blocks-test-edit-content-link"] .dropbutton a[href="/block/' . $block_content->id() . '"]');

    // Clean
    $block->delete();
    $block_content->delete();
    $this->drupalLogout();
  }

  /**
   * Test content block form autofocus.
   *
   * @covers ::improvements_form_block_content_form_alter
   */
  private function _testBlockContentAutofocus(): void {
    $this->drupalLoginAsRoot();
    $this->drupalGet('/block/add/basic');
    $this->dontSeeErrorMessage();
    $this->assertSession()->elementAttributeExists('css', '#edit-info-0-value', 'autofocus');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test node improvements.
   */
  private function _testNodeImprovements(): void {
    $this->drupalLoginAsRoot();
    $web_assert = $this->assertSession();

    // Test "Node form" without advances settings
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#edit-revision-information');
    $web_assert->elementExists('css', '.entity-content-form-footer .field--name-status');
    $web_assert->elementAttributeExists('css', '#edit-title-0-value', 'autofocus');

    // Test "Node type form"
    $this->drupalGet('/admin/structure/types/manage/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', '#edit-hide-revision-group');
    $web_assert->elementExists('css', '#edit-move-status-to-vertical-tab');
    $web_assert->checkboxChecked('menu_options[main]');

    // Test change advanced settings in "Node type form"
    $this->submitForm([
      'options[revision]' => FALSE,
      'hide_revision_group' => TRUE,
      'move_status_to_vertical_tab' => TRUE,
    ], 'Save content type');
    $this->dontSeeErrorMessage();
    $this->drupalGet('/admin/structure/types/manage/page');
    $this->dontSeeErrorMessage();
    $web_assert->checkboxChecked('hide_revision_group');
    $web_assert->checkboxChecked('move_status_to_vertical_tab');

    // Test "Node form" with advanced settings
    $this->drupalGet('/node/add/page');
    $this->dontSeeErrorMessage();
    $web_assert->elementNotExists('css', '#edit-revision-information');
    $web_assert->elementNotExists('css', '.entity-content-form-footer .field--name-status');
    $web_assert->elementExists('css', '#edit-options .field--name-status');

    // Test "Node form" default settings
    $this->drupalGet('/admin/structure/types/add');
    $this->dontSeeErrorMessage();
    // @TODO Fix
    //$web_assert->checkboxNotChecked('menu_options[main]');
    $web_assert->elementAttributeExists('css', '#edit-preview-mode-0', 'checked');
    $web_assert->checkboxNotChecked('options[promote]');
    $web_assert->checkboxNotChecked('options[revision]');
    $web_assert->checkboxNotChecked('display_submitted');

    // Test "Title" field in display options
    $this->drupalGet('/admin/structure/types/manage/page/display');
    $this->dontSeeErrorMessage();
    $web_assert->elementExists('css', 'tr#title');
    $web_assert->fieldExists('fields[title][type]');
    // @TODO Check render field in node page

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test third party site information.
   */
  private function _testSiteInformationThirdPartySettings(): void {
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/config/system/site-information');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldExists('company_name');
    $web_assert->fieldExists('mail_for_notifications');
    $web_assert->fieldExists('phone');

    $this->submitForm([
      'company_name' => 'OOO ' . __FUNCTION__,
      'mail_for_notifications' => __FUNCTION__ . '@example.com',
      'phone' => '123456789',
    ], 'Save configuration');
    $this->drupalGet('/admin/config/system/site-information');
    $this->dontSeeErrorMessage();
    $web_assert->fieldValueEquals('company_name', 'OOO ' . __FUNCTION__);
    $web_assert->fieldValueEquals('mail_for_notifications', __FUNCTION__ . '@example.com');
    $web_assert->fieldValueEquals('phone', '123456789');

    // Clean
    $this->drupalLogout();
  }

  /**
   * Test cookies_hash cache context.
   *
   * @covers \Drupal\improvements\Cache\CookiesHashCacheContext
   */
  private function _testCacheContextCookiesHash(): void {
    $request_stack = new RequestStack();
    $request = Request::create(
      uri: '/',
      cookies: ['foo' => 'bar'],
    );
    $request_stack->push($request);
    $cache_context = new CookiesHashCacheContext($request_stack);
    $this->assertSame('64b41e5d', $cache_context->getContext());
    $this->assertSame('f4bb71a2', $cache_context->getContext('foo'));

    $request_stack = new RequestStack();
    $request = Request::create('/');
    $request_stack->push($request);
    $cache_context = new CookiesHashCacheContext($request_stack);
    $this->assertSame('', $cache_context->getContext());
    $this->assertSame('', $cache_context->getContext('foo'));
  }

  /**
   * Test entity_field cache context.
   *
   * @covers \Drupal\improvements\Cache\EntityFieldCacheContext
   */
  private function _testCacheContextEntityField(): void {
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'uid' => 1,
    ]);
    $route = new Route('/node/{node}');
    $route_match = new RouteMatch('entity.node.canonical', $route, ['node' => $node], ['node' => $node->id()]);
    $cache_context = new EntityFieldCacheContext($route_match);
    $this->assertSame((string)$node->get('uid')->target_id, $cache_context->getContext('uid'));
    $this->assertSame($node->label(), $cache_context->getContext('title'));
    $this->assertSame('', $cache_context->getContext('dummy'));

    $route = new Route('/');
    $route_match = new RouteMatch('<front>', $route);
    $cache_context = new EntityFieldCacheContext($route_match);
    $this->assertSame('', $cache_context->getContext('title'));
    $this->assertSame('', $cache_context->getContext('dummy'));

    // Clean
    $node->delete();
  }

  /**
   * Test entity_type cache context.
   *
   * @covers \Drupal\improvements\Cache\EntityTypeCacheContext
   */
  private function _testCacheContextEntityType(): void {
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'uid' => 1,
    ]);
    $route = new Route('/node/{node}');
    $route_match = new RouteMatch('entity.node.canonical', $route, ['node' => $node], ['node' => $node->id()]);
    $cache_context = new EntityTypeCacheContext($route_match);
    $this->assertSame('node', $cache_context->getContext());

    $route = new Route('/');
    $route_match = new RouteMatch('<front>', $route);
    $cache_context = new EntityTypeCacheContext($route_match);
    $this->assertSame('', $cache_context->getContext());

    // Clean
    $node->delete();
  }

  /**
   * Test is_entity_page cache context.
   *
   * @covers \Drupal\improvements\Cache\IsEntityPageCacheContext
   */
  private function _testCacheContextIsEntityPage(): void {
    $node = $this->createNode([
      'title' => 'Node for test ' . __FUNCTION__,
      'uid' => 1,
    ]);
    $route = new Route('/node/{node}');
    $route_match = new RouteMatch('entity.node.canonical', $route, ['node' => $node], ['node' => $node->id()]);
    $cache_context = new IsEntityPageCacheContext($route_match);
    $this->assertSame('1', $cache_context->getContext());
    $this->assertSame('1', $cache_context->getContext('node'));
    $this->assertSame('1', $cache_context->getContext('node,taxonomy_term'));
    $this->assertSame('0', $cache_context->getContext('taxonomy_term'));

    $route = new Route('/');
    $route_match = new RouteMatch('<front>', $route);
    $cache_context = new IsEntityPageCacheContext($route_match);
    $this->assertSame('0', $cache_context->getContext());
    $this->assertSame('0', $cache_context->getContext('node'));
    $this->assertSame('0', $cache_context->getContext('node,taxonomy_term'));
    $this->assertSame('0', $cache_context->getContext('taxonomy_term'));

    // Clean
    $node->delete();
  }

  /**
   * Test hook hook_page_ROUTE_NAME_result_alter().
   *
   * @covers \Drupal\improvements\ImprovementsEventSubscriber::onKernelView
   */
  private function _testHookPageResultAlter(): void {
    $this->drupalGet('/improvements-test/altered-page-result');
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('Altered result');
    $web_assert->pageTextNotContains('Markup from controller');
  }

  /**
   * Test filter plugin "nofollow_external_links_filter".
   *
   * @covers \Drupal\improvements\Plugin\Filter\NofollowExternalLinksFilter
   */
  private function _testFilterPluginNofollowExternalLinks(): void {
    $filter = $this->createFilterFormat([
      'format' => 'test_nofollow_external_links',
      'name' => 'Test nofollow_external_links_filter',
      'filters' => [
        'nofollow_external_links_filter' => [
          'status' => 1,
        ],
      ],
    ]);
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/config/content/formats/manage/' . $filter->id());
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldExists('filters[nofollow_external_links_filter][status]');
    $web_assert->checkboxChecked('filters[nofollow_external_links_filter][status]');

    $result = check_markup('foo <a href="https://google.com">Google</a> bar', $filter->id());
    $this->assertSame('foo <a href="https://google.com" rel="nofollow">Google</a> bar', (string)$result);

    // Clean
    $this->deleteEntities($filter);
    $this->drupalLogout();
  }

  /**
   * Test filter plugin "table_wrapper_filter".
   *
   * @covers \Drupal\improvements\Plugin\Filter\TableWrapperFilter
   */
  private function _testFilterPluginTableWrapper(): void {
    $filter = $this->createFilterFormat([
      'format' => 'table_wrapper_filter',
      'name' => 'Test table_wrapper_filter',
      'filters' => [
        'table_wrapper_filter' => [
          'status' => 1,
        ],
      ],
    ]);
    $this->drupalLoginAsRoot();
    $this->drupalGet('/admin/config/content/formats/manage/' . $filter->id());
    $this->dontSeeErrorMessage();
    $web_assert = $this->assertSession();
    $web_assert->fieldExists('filters[table_wrapper_filter][status]');
    $web_assert->checkboxChecked('filters[table_wrapper_filter][status]');

    $result = check_markup('foo <table></table> bar', $filter->id());
    $this->assertSame('foo <div class="table-wrapper"><table></table></div> bar', (string)$result);

    // Clean
    $this->deleteEntities($filter);
    $this->drupalLogout();
  }

  /**
   * Test access check by uuid.
   *
   * @covers \Drupal\improvements\Access\EntityUuidAccessCheck
   */
  private function _testAccessCheckEntityUuid(): void {
    $web_assert = $this->assertSession();
    $node = $this->createNode(['title' => 'Node for test ' . __FUNCTION__]);

    $this->drupalGet('/improvements-test/entity-uuid-access-check/' . $node->id());
    $web_assert->statusCodeEquals(403);

    $this->drupalGet('/improvements-test/entity-uuid-access-check/' . $node->id(), ['query' => ['uuid' => '123']]);
    $web_assert->statusCodeEquals(403);

    $this->drupalGet('/improvements-test/entity-uuid-access-check/' . $node->id(), ['query' => ['uuid' => 'true']]);
    $web_assert->statusCodeEquals(403);

    $this->drupalGet('/improvements-test/entity-uuid-access-check/' . $node->id(), ['query' => ['uuid' => '1']]);
    $web_assert->statusCodeEquals(403);

    $this->drupalGet('/improvements-test/entity-uuid-access-check/' . $node->id(), ['query' => ['uuid' => '0']]);
    $web_assert->statusCodeEquals(403);

    $this->drupalGet('/improvements-test/entity-uuid-access-check/' . $node->id(), ['query' => ['uuid' => $node->uuid()]]);
    $web_assert->statusCodeEquals(200);

    // Clean
    $this->deleteEntities($node);
  }

  /**
   * Test token [current-page:url-with-query]
   */
  private function _testTokenCurrentPageUrlWithQuery(): void {
    $request = Request::create('/admin', 'GET', ['foo' => 'bar']);
    $request_stack = $this->container->get('request_stack');
    $request_stack->push($request);

    $this->assertStringContainsString('/admin?foo=bar', $this->tokenService->replace('[current-page:url-with-query]'));
    $this->assertSame('/admin?foo=bar', $this->tokenService->replace('[current-page:url-with-query:relative]'));

    $request_stack->pop();
  }

  /**
   * Test tokens [site:third_party_setting_name].
   */
  private function _testTokenSiteThirdPartySettings(): void {
    $this->configFactory->getEditable('system.site')
      ->set('improvements.company_name', 'OOO ' . __FUNCTION__)
      ->set('improvements.mail_for_notifications', __FUNCTION__ . '@example.com')
      ->set('improvements.phone', '321654987')
      ->save();
    $this->assertEquals('OOO ' . __FUNCTION__, $this->tokenService->replace('[site:company_name]'));
    $this->assertEquals(__FUNCTION__ . '@example.com', $this->tokenService->replace('[site:mail_for_notifications]'));
    $this->assertEquals('321654987', $this->tokenService->replace('[site:phone]'));
  }

  /**
   * Test ajax commands.
   */
  private function _testAjaxCommands(): void {
    (new AddClassCommand('.selector', 'class-name'))->render();
    (new ConsoleLogCommand('message'))->render();
    (new FormResetCommand('.form-selector', TRUE))->render();
    (new HistoryReplaceStateCommand('/new-url'))->render();
    (new PageReloadCommand())->render();
    (new RemoveClassCommand('.selector', 'class-name'))->render();
  }

  /**
   * Test DefaultDraggableListBuilder.
   *
   * @covers \Drupal\improvements\DefaultDraggableListBuilder
   */
  private function _testDefaultDraggableListBuilder(): void {
    $web_assert = $this->assertSession();
    $this->drupalLoginAsRoot();

    $entity1 = TestConfigEntity::create(['id' => 'ent1', 'label' => 'Test entity 1 for ' . __FUNCTION__, 'weight' => 1]);
    $entity1->save();
    $entity2 = TestConfigEntity::create(['id' => 'ent2', 'label' => 'Test entity 2 for ' . __FUNCTION__, 'weight' => 2]);
    $entity2->save();

    $this->drupalGet(Url::fromRoute('entity.test_config_entity.collection'));
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($entity1->label());
    $web_assert->pageTextContains($entity2->label());
    $web_assert->fieldValueEquals('entities[' . $entity1->id() . '][weight]', $entity1->get('weight'));
    $web_assert->fieldValueEquals('entities[' . $entity2->id() . '][weight]', $entity2->get('weight'));

    $this->submitForm([
      'entities[' . $entity1->id() . '][weight]' => 2,
      'entities[' . $entity2->id() . '][weight]' => 1,
    ], 'Save');
    $this->dontSeeErrorMessage();
    $web_assert->fieldValueEquals('entities[' . $entity1->id() . '][weight]', 2);
    $web_assert->fieldValueEquals('entities[' . $entity2->id() . '][weight]', 1);

    // Clean
    $this->deleteEntities($entity1, $entity2);
    $this->drupalLogout();
  }

  /**
   * Test DefaultConfigEntityListBuilder.
   *
   * @covers \Drupal\improvements\DefaultConfigEntityListBuilder
   */
  private function _testDefaultConfigEntityListBuilder(): void {
    $web_assert = $this->assertSession();
    $this->drupalLoginAsRoot();

    $entity1 = SecondConfigEntity::create(['id' => 'ent1', 'label' => 'Entity 1 for ' . __FUNCTION__]);
    $entity1->save();
    $entity2 = SecondConfigEntity::create(['id' => 'ent2', 'label' => 'Entity 2 for ' . __FUNCTION__]);
    $entity2->save();

    $this->drupalGet(Url::fromRoute('entity.second_config_entity.collection'));
    $this->dontSeeErrorMessage();
    $web_assert->pageTextContains($entity1->label());
    $web_assert->pageTextContains($entity2->label());

    // Clean
    $this->deleteEntities($entity1, $entity2);
    $this->drupalLogout();
  }

}
