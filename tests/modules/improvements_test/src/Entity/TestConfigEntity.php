<?php

namespace Drupal\improvements_test\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * @ConfigEntityType(
 *   id = "test_config_entity",
 *   label = @Translation("Test config entity"),
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *   },
 *   handlers = {
 *     "list_builder" = "\Drupal\improvements\DefaultDraggableListBuilder",
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/test_config_entities",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *   },
 * )
 */
class TestConfigEntity extends ConfigEntityBase {

  protected $id;

  protected $label;

  protected $weight;

}
