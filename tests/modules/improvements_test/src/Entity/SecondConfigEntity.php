<?php

namespace Drupal\improvements_test\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * @ConfigEntityType(
 *   id = "second_config_entity",
 *   label = @Translation("Second config entity"),
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   handlers = {
 *     "list_builder" = "\Drupal\improvements\DefaultConfigEntityListBuilder",
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/second_config_entities",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 * )
 */
class SecondConfigEntity extends ConfigEntityBase {

  protected $id;

  protected $label;

}
