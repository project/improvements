<?php

namespace Drupal\improvements_test\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

class ImprovementsTestForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'improvements_test_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['select1'] = [
      '#type' => 'select',
      '#title' => 'Default select',
      '#options' => [
        'foo' => 'Foo',
        'bar' => 'Bar',
      ],
    ];

    $form['select2'] = [
      '#type' => 'select',
      '#title' => 'Select with attributes',
      '#options' => [
        'foo' => 'Foo',
        'bar' => 'Bar',
        'baz' => 'Baz',
      ],
      '#options_attributes' => [
        'foo' => [
          'class' => ['foo-test-class'],
          'data-custom-attribute' => 'foo-custom-attribute',
        ],
        'bar' => [
          'disabled' => TRUE,
        ],
      ],
    ];

    $form['label_in_placeholder'] = [
      '#type' => 'textfield',
      '#title' => 'Label in placeholder',
      '#title_display' => 'placeholder',
    ];

    $form['file_without_autoupload'] = [
      '#type' => 'managed_file',
      '#title' => 'File without autoupload',
      '#autoupload' => FALSE,
    ];

    $form['file_with_button'] = [
      '#type' => 'managed_file',
      '#title' => 'File with button',
      '#autoupload' => FALSE,
      '#upload_button_state' => 1,
    ];

    $form['file_without_button'] = [
      '#type' => 'managed_file',
      '#title' => 'File without button',
      '#autoupload' => FALSE,
      '#upload_button_state' => 0,
    ];

    $form['range_slider'] = [
      '#type' => 'range_slider',
      '#title' => 'Range slider',
      '#step' => 1,
      '#min' => 0,
      '#max' => 100,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    $form['#tracking_submit'] = TRUE;
    $form['#cache']['max-age'] = 0;

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addMessage(Markup::create('<pre>' . Html::escape(print_r($form_state->getValues(), TRUE)) . '</pre>'));
  }

}
