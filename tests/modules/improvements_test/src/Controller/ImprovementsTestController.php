<?php

namespace Drupal\improvements_test\Controller;

use Drupal\contact\ContactFormInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\improvements\ImprovementsHelper;
use Drupal\node\NodeInterface;

class ImprovementsTestController extends ControllerBase {

  public function alteredRouteTitle() {
    return [
      '#markup' => 'Hello, world!',
    ];
  }

  public function alteredPageResult() {
    return [
      '#markup' => 'Markup from controller',
    ];
  }

  public function contactForm(ContactFormInterface $contact_form) {
    return improvements_contact_get_message_form($contact_form->id());
  }

  public function entityUuidAccessCheck(NodeInterface $node) {
    return [
      '#markup' => 'Hello World!',
    ];
  }

}
