<?php

namespace Drupal\improvements;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Markup;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

class ImprovementsTrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'pagePreRender',
      'linkPostRender',
    ];
  }

  /**
   * "Page" element pre render callback.
   *
   * @see improvements_element_info_alter()
   */
  public static function pagePreRender(array $element): array {
    // Remove "off canvas" wrapper for non-admin users.
    if (!\Drupal::currentUser()->hasPermission('administer site configuration')) {
      unset($element['#theme_wrappers']['off_canvas_page_wrapper']);
    }

    return $element;
  }

  /**
   * "Link" element post render callback.
   *
   * @see improvements_element_info_alter()
   */
  public static function linkPostRender($markup, array $element) {
    // Add support empty link fragment
    if (
      isset($element['#url']) &&
      $element['#url'] instanceof Url &&
      $element['#url']->getOption('fragment') == '<none>'
    ) {
      $markup = Markup::create(str_replace('href="#' . Html::escape('<none>') . '"', 'href="#"', $markup));
    }

    return $markup;
  }

}
