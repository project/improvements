<?php

namespace Drupal\improvements\Cache;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Cache context ID: 'entity_type' (to vary by current entity type).
 */
class EntityTypeCacheContext implements CacheContextInterface {

  protected RouteMatchInterface $routeMatch;

  /**
   * Class constructor.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel(): MarkupInterface {
    return t('Entity type');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): string {
    if (preg_match('/^entity\.(.+)\.canonical$/', $this->routeMatch->getRouteName(), $matches)) {
      return $matches[1];
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return new CacheableMetadata();
  }

}
