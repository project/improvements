<?php

namespace Drupal\improvements\Cache;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\Context\CookiesCacheContext;

/**
 * Cache context ID: 'cookies_hash' (to vary by all cookies hash).
 * Calculated cache context ID: 'cookies_hash:%name', e.g.'cookies_hash:device_type' (to vary by the 'device_type' cookie hash).
 */
class CookiesHashCacheContext extends CookiesCacheContext {

  /**
   * {@inheritDoc}
   */
  public function getContext($cookie = NULL): string {
    $result = parent::getContext($cookie);
    return ($result !== NULL && $result !== '' && $result !== []) ? hash('crc32', Json::encode($result)) : '';
  }

}
