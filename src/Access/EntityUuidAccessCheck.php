<?php

namespace Drupal\improvements\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

class EntityUuidAccessCheck implements AccessInterface {

  /**
   * Checks access based on a uuid $_GET parameter for the request.
   */
  public function access(Route $route, RouteMatchInterface $route_match, Request $request): AccessResultInterface {
    if ($request_uuid = $request->query->get('uuid')) {
      $entity = $this->getEntityFromRoute($route_match);
      if ($entity && $request_uuid == $entity->uuid()) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Return entity from route.
   */
  protected function getEntityFromRoute(RouteMatchInterface $route_match): ?EntityInterface {
    foreach ($route_match->getParameters() as $route_parameter) {
      if ($route_parameter instanceof EntityInterface) {
        return $route_parameter;
      }
    }
    return NULL;
  }

}
