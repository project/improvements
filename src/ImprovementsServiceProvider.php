<?php

namespace Drupal\improvements;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class ImprovementsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container): void {
    // Remove event subscriber service response_filter.active_link
    // @see \Drupal\Core\EventSubscriber\ActiveLinkResponseFilter
    $container->removeDefinition('response_filter.active_link');
  }

}
