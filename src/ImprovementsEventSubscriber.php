<?php

namespace Drupal\improvements;

use Drupal\Core\Routing\RouteObjectInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber MyEventSubscriber.
 */
class ImprovementsEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::VIEW][] = ['onKernelView', 10];
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 10];
    return $events;
  }

  /**
   * KernelEvents::VIEW event callback.
   */
  public function onKernelView(ViewEvent $event): void {
    // Create hook_page_ROUTE_NAME_result_alter()
    $result = $event->getControllerResult();
    $current_route_name = \Drupal::routeMatch()->getRouteName();
    \Drupal::moduleHandler()->alter('page_' . str_replace('.', '_', $current_route_name) . '_result', $result);
    $event->setControllerResult($result);
  }

  /**
   * KernelEvents::RESPONSE event callback.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    // Clear page cache on clear render cache
    $request = $event->getRequest();
    $route_name = $request->attributes->get(RouteObjectInterface::ROUTE_NAME);
    if ($route_name == 'admin_toolbar_tools.flush_rendercache') {
      $database = \Drupal::database();
      if ($database->schema()->tableExists('cache_page')) {
        $database->truncate('cache_page')->execute();
      }
    }
  }

}
