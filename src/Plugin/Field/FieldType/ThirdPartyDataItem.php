<?php

namespace Drupal\improvements\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\MapItem;

/**
 * @FieldType(
 *   id = "third_party_data",
 *   label = @Translation("Third party data"),
 *   description = @Translation("Field for storing modules data."),
 *   cardinality = 1,
 *   no_ui = TRUE,
 *   list_class = "\Drupal\improvements\Plugin\Field\ThirdPartyDataItemList",
 * )
 */
class ThirdPartyDataItem extends MapItem { }
