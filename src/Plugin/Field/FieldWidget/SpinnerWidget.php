<?php

namespace Drupal\improvements\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * @FieldWidget(
 *   id = "spinner",
 *   label = @Translation("Spinner"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float",
 *   }
 * )
 *
 * @see \Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget
 */
class SpinnerWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'min' => NULL,
      'max' => NULL,
      'step' => 1,
      'hide_label' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $field_settings = $this->getFieldSettings();

    $element['min'] = [
      '#type' => 'textfield',
      '#title' => t('Minimum'),
      '#default_value' => $this->getSetting('min'),
      '#description' => t('Minimum value. Keep empty to use field storage settings.'),
      '#placeholder' => $field_settings['min'],
      '#size' => 10,
    ];

    $element['max'] = [
      '#type' => 'textfield',
      '#title' => t('Maximum'),
      '#default_value' => $this->getSetting('max'),
      '#description' => t('Maximum value. Keep empty to use field storage settings.'),
      '#placeholder' => $field_settings['max'],
      '#size' => 10,
    ];

    $element['step'] = [
      '#type' => 'textfield',
      '#title' => t('Step'),
      '#default_value' => $this->getSetting('step'),
      '#size' => 10,
    ];

    $element['hide_label'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide label'),
      '#default_value' => $this->getSetting('hide_label'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $widget_settings = $this->getSettings();
    return [
      t('Minimum') . ': ' . ($widget_settings['min'] === NULL || $widget_settings['min'] === '' ? 'default' : $widget_settings['min']),
      t('Maximum') . ': ' . ($widget_settings['max'] === NULL || $widget_settings['max'] === '' ? 'default' : $widget_settings['max']),
      t('Step') . ': ' . $this->getSetting('step'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $widget_settings = $this->getSettings();
    $field_settings = $this->getFieldSettings();

    if (!is_numeric($widget_settings['min']) && is_numeric($field_settings['min'])) {
      $widget_settings['min'] = $field_settings['min'];
    }
    if (!is_numeric($widget_settings['max']) && is_numeric($field_settings['max'])) {
      $widget_settings['max'] = $field_settings['max'];
    }

    $element += [
      '#type' => 'spinner',
      '#min' => $widget_settings['min'],
      '#max' => $widget_settings['max'],
      '#step' => $widget_settings['step'],
      '#default_value' => $items[$delta]->value ?? NULL,
    ];

    if ($widget_settings['hide_label']) {
      $element['#title_display'] = 'invisible';
    }

    return [
      'value' => $element,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return $element['value'];
  }

}
