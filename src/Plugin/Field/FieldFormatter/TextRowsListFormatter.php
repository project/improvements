<?php

namespace Drupal\improvements\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldFormatter(
 *   id = "text_rows_list_formatter",
 *   label = @Translation("Text rows list"),
 *   field_types = {
 *     "string_long",
 *     "text_long",
 *   },
 * )
 */
class TextRowsListFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $options = parent::defaultSettings();

    $options['list_class'] = '';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['list_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List css class'),
      '#default_value' => $this->getSetting('list_class'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    if ($list_class = $this->getSetting('list_class')) {
      $summary[] = $this->t('List css class') . ': ' . $list_class;
    }

    return $summary;
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $attributes = [];
    if ($list_class = $this->getSetting('list_class')) {
      $attributes['class'] = explode(' ', $list_class);
    }

    foreach ($items as $delta => $item) {
      $text_rows = explode("\n", trim($item->value));
      $text_rows = array_map('trim', $text_rows);

      $elements[$delta] = [
        '#theme' => 'item_list',
        '#items' => $text_rows,
        '#attributes' => $attributes,
      ];
    }

    return $elements;
  }

}
