<?php

namespace Drupal\improvements\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter;

/**
 * @FieldFormatter(
 *   id = "human_readable_decimal_formatter",
 *   label = @Translation("Human-readable decimal formatter"),
 *   field_types = {
 *     "decimal",
 *     "float",
 *   }
 * )
 */
class HumanReadableDecimalFormatter extends DecimalFormatter {

  /**
   * {@inheritDoc}
   */
  protected function numberFormat($number): string {
    return rtrim(rtrim(parent::numberFormat($number), '0'), '.');
  }

}
