<?php

namespace Drupal\improvements\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * @FieldFormatter(
 *   id = "link_custom_text",
 *   label = @Translation("Link with custom text"),
 *   field_types = {
 *     "link",
 *   },
 * )
 */
class LinkCustomTextFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'text' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    // @TODO Wait https://www.drupal.org/project/drupal/issues/2546212
  }

}
