<?php

namespace Drupal\improvements\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldFormatter(
 *   id = "string_list_formatter",
 *   label = @Translation("List"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class StringListFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $options = parent::defaultSettings();

    $options['list_class'] = '';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['list_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List css class'),
      '#default_value' => $this->getSetting('list_class'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    if ($list_class = $this->getSetting('list_class')) {
      $summary[] = $this->t('List css class') . ': ' . $list_class;
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $values = array_map(function (FieldItemInterface $item) {
      return $item->value;
    }, iterator_to_array($items));

    if ($values) {
      $attributes = [];
      if ($list_class = $this->getSetting('list_class')) {
        $attributes['class'] = explode(' ', $list_class);
      }

      return [[
        '#theme' => 'item_list',
        '#items' => $values,
        '#attributes' => $attributes,
      ]];
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getFieldStorageDefinition()->isMultiple();
  }

}
