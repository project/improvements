<?php

namespace Drupal\improvements\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;

/**
 * @FieldFormatter(
 *   id = "text_without_escaping",
 *   label = @Translation("Text without escaping"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class TextWithoutEscapingFormatter extends StringFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewValue(FieldItemInterface $item): array {
    return [
      '#markup' => Xss::filterAdmin($item->value),
    ];
  }

}
