<?php

namespace Drupal\improvements\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;

/**
 * @FieldFormatter(
 *   id = "extended_image_formatter",
 *   label = @Translation("Extended image formatter"),
 *   field_types = {
 *     "image",
 *   },
 * )
 */
class ExtendedImageFormatter extends ImageFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'image_style' => '',
      'first_image_style' => '',
      'image_link' => '',
      'image_link_style' => '',
      'items_limit' => 0,
      'default_image_type' => 'default',
      'default_image_custom' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $image_styles = image_style_options(FALSE);

    $element['image_style'] = [
      '#type' => 'select',
      '#title' => t('Image style'),
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['first_image_style'] = [
      '#type' => 'select',
      '#title' => t('First image style'),
      '#default_value' => $this->getSetting('first_image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['image_link'] = [
      '#type' => 'select',
      '#title' => t('Link image to'),
      '#default_value' => $this->getSetting('image_link'),
      '#empty_option' => t('Nothing'),
      '#options' => [
        'content' => t('Content'),
        'file' => t('File'),
      ],
    ];

    $element['image_link_style'] = [
      '#type' => 'select',
      '#title' => t('Image link style'),
      '#default_value' => $this->getSetting('image_link_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    ];

    $element['default_image_type'] = [
      '#type' => 'select',
      '#title' => t('Default image'),
      '#options' => [
        'default' => t('Default'),
        'custom' => t('Custom'),
      ],
      '#default_value' => $this->getSetting('default_image_type'),
    ];

    $element['default_image_custom'] = [
      '#type' => 'textfield',
      '#title' => t('Custom default image'),
      '#default_value' => $this->getSetting('default_image_custom'),
    ];

    $element['items_limit'] = [
      '#type' => 'number',
      '#title' => t('Items limit'),
      '#min' => 0,
      '#default_value' => $this->getSetting('items_limit'),
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsSummary(): array {
    $summary[] = t('Image style') . ': ' . ($this->getSetting('image_style') ?: '~');
    $summary[] = t('First image style') . ': ' . ($this->getSetting('first_image_style') ?: '~');
    $summary[] = t('Link image to') . ': ' . ($this->getSetting('image_link') ?: '~');
    $summary[] = t('Image link style') . ': ' . ($this->getSetting('image_link_style') ?: '~');
    $summary[] = t('Default image type') . ': ' . $this->getSetting('default_image_type');
    $summary[] = t('Items limit') . ': ' . $this->getSetting('items_limit');

    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter::viewElements()
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $formatter_settings = $this->getSettings();
    $elements = [];

    // Custom default image
    if ($items->isEmpty() && $formatter_settings['default_image_type'] == 'custom') {
      $elements[0] = [
        '#markup' => $formatter_settings['default_image_custom'],
      ];
      return $elements;
    }

    $files = $this->getEntitiesToView($items, $langcode);

    if (!$files) {
      return $elements;
    }

    $image_style_setting = $formatter_settings['image_style'];
    $first_image_style_setting = $formatter_settings['first_image_style'];
    $image_link_setting = $formatter_settings['image_link'];
    $image_link_style_setting = $formatter_settings['image_link_style'];
    $image_link_style = $image_link_style_setting ? ImageStyle::load($image_link_style_setting) : NULL; /** @var ImageStyleInterface $image_link_style */
    $items_limit = $formatter_settings['items_limit'];

    // Items limit
    if ($items_limit && count($files) > $items_limit) {
      $files = array_slice($files, 0, $items_limit);
    }

    // Get image link url
    $image_link_url = NULL;
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $image_link_url = $entity->toUrl();
      }
    }

    // Collect cache tags to be added for each item in the field
    $cache_tags = $this->getImageStylesCacheTags($image_style_setting, $first_image_style_setting);

    foreach ($files as $delta => $file) {
      $cache_contexts = [];
      if ($image_link_setting == 'file') {
        // @todo Wrap in file_url_transform_relative(). This is currently
        // impossible. As a work-around, we currently add the 'url.site' cache
        // context to ensure different file URLs are generated for different
        // sites in a multisite setup, including HTTP and HTTPS versions of the
        // same site. Fix in https://www.drupal.org/node/2646744.
        if ($image_link_style) {
          $image_link_url = $image_link_style->buildUrl($file->getFileUri());
        }
        else {
          $image_link_url = file_create_url($file->getFileUri());
        }

        $image_link_url = Url::fromUri($image_link_url);
        $image_link_url->setOption('attributes', ['target' => '_blank']);

        $cache_contexts[] = 'url.site';
      }

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $elements[$delta] = [
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => ($first_image_style_setting && $delta == 0) ? $first_image_style_setting : $image_style_setting,
        '#url' => $image_link_url,
        '#cache' => [
          'tags' => Cache::mergeTags($cache_tags, $file->getCacheTags()),
          'contexts' => $cache_contexts,
        ],
      ];
    }

    return $elements;
  }

  /**
   * Return image styles cache tags.
   */
  protected function getImageStylesCacheTags(): array {
    $cache_tags = [];

    foreach (func_get_args() as $image_style_name) {
      if ($image_style_name) {
        $image_style = ImageStyle::load($image_style_name);
        if ($image_style_cache_tags = $image_style->getCacheTags()) {
          $cache_tags = Cache::mergeTags($cache_tags, $image_style_cache_tags);
        }
      }
    }

    return $cache_tags;
  }

}
