<?php

namespace Drupal\improvements\Plugin\Field;

use Drupal\Core\Field\FieldItemList;

class ThirdPartyDataItemList extends FieldItemList {

  /**
   * {@inheritDoc}
   */
  public function getValue(): ?array {
    if (!$this->isEmpty()) {
      return $this->first()->getValue();
    }
    return NULL;
  }

  /**
   * Return third party data.
   */
  public function getThirdPartyData(string $module, $key = NULL, $default = NULL) {
    $thid_party_data = $this->getValue();
    if ($key !== NULL) {
      return $thid_party_data[$module][$key] ?? $default;
    }
    return $thid_party_data[$module] ?? $default;
  }

  /**
   * Store third party data.
   */
  public function setThirdPartyData(string $module, $key, $data): void {
    $third_party_data = $this->getValue();
    if ($key !== NULL) {
      $third_party_data[$module][$key] = $data;
    }
    else {
      $third_party_data[$module] = $data;
    }
    $this->setValue($third_party_data);
  }

}
