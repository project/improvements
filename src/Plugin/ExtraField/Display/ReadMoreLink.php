<?php

namespace Drupal\improvements\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field_plus\Plugin\ExtraFieldPlusDisplayFormattedBase;

/**
 * @ExtraFieldDisplay(
 *   id = "read_more_link",
 *   label = @Translation("Read More link"),
 *   bundles = {
 *     "node.*",
 *     "taxonomy_term.*",
 *   },
 *   visible = false,
 * )
 */
class ReadMoreLink extends ExtraFieldPlusDisplayFormattedBase {

  /**
   * {@inheritdoc}
   */
  protected static function defaultExtraFieldSettings(): array {
    $values = parent::defaultExtraFieldSettings();

    $values += [
      'text' => 'Read more',
    ];

    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public static function extraFieldSettingsForm(): array {
    $form = [];

    $form['text'] = [
      '#type' => 'textfield',
      '#title' => t('Link text'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(ContentEntityInterface $entity): array {
    $formatter_settings = $this->getEntityExtraFieldSettings();

    return [
      '#type' => 'link',
      '#title' => t($formatter_settings['text']),
      '#url' => $entity->toUrl(),
    ];
  }

}
