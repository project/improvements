<?php

namespace Drupal\improvements\Plugin\Condition;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\druhels\ArrayHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Route' condition.
 *
 * @Condition(
 *   id = "route",
 *   label = @Translation("Route"),
 * )
 */
class RouteCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   */
  protected RouteMatchInterface $currentRouteMatch;

  /**
   * Constructs a Route condition plugin.
   */
  public function __construct(RouteMatchInterface $current_route_match, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('current_route_match'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['routes' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['routes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Routes'),
      '#default_value' => $this->configuration['routes'],
      '#description' => $this->t('Enter one route per line.'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['routes'] = $form_state->getValue('routes');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if (empty($this->configuration['routes'])) {
      return $this->t('No routes is specified');
    }

    $routes = implode(', ', $this->getConfigurationRoutes());

    if (!empty($this->configuration['negate'])) {
      return $this->t('Do not return true on the following routes: @routes', ['@routes' => $routes]);
    }
    return $this->t('Return true on the following routes: @routes', ['@routes' => $routes]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $routes = $this->getConfigurationRoutes();

    if (!$routes) {
      return TRUE;
    }

    return in_array($this->currentRouteMatch->getRouteName(), $routes, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route.name']);
  }

  /**
   * Return configuration routes array.
   */
  protected function getConfigurationRoutes(): array {
    return $this->configuration['routes'] ? ArrayHelper::getTextLines($this->configuration['routes']) : [];
  }

}
