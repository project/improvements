<?php

namespace Drupal\improvements\Plugin\migrate\process;

use Drupal\Core\Database\Connection;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_entity_metatag"
 * )
 */
class D7EntityMetatag extends ProcessPluginBase {

  protected MigrationInterface $migration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migration = $migration;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property): string {
    $source_database = $this->migration->getSourcePlugin()->getDatabase(); /** @var Connection $source_database */

    $metatag = $source_database->select('metatag')
      ->fields('metatag', ['data'])
      ->condition('metatag.entity_type', $this->configuration['entity_type'])
      ->condition('metatag.entity_id', $value)
      ->execute()
      ->fetchField();

    return $metatag ? $metatag : 'a:0:{}';
  }

}
