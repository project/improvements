<?php

namespace Drupal\improvements\Plugin\migrate\process;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_file_lookup"
 * )
 */
class D7FileLookup extends ProcessPluginBase {

  /**
   * @var MigrationInterface
   */
  protected $migration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migration = $migration;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source_database = $this->migration->getSourcePlugin()->getDatabase(); /** @var Connection $source_database */
    $file = $source_database->select('file_managed', 'f')
      ->fields('f', ['filename', 'uri'])
      ->condition('f.fid', $value)
      ->execute()
      ->fetchAssoc();

    if ($file) {
      $filename_pathinfo = pathinfo($file['filename']);
      $file['extension'] = $filename_pathinfo['extension'];

      return $file;
    }
  }

}
