<?php

namespace Drupal\improvements\Plugin\migrate\process;

use Drupal\Core\Database\Connection;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_entity_path_alias"
 * )
 */
class D7EntityPathAlias extends ProcessPluginBase {

  /**
   * @var MigrationInterface
   */
  protected $migration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migration = $migration;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source_database = $this->migration->getSourcePlugin()->getDatabase(); /** @var Connection $source_database */
    $path_alias = $source_database->select('url_alias', 'alias')
      ->fields('alias', ['alias'])
      ->condition('alias.source', str_replace('%', $value, $this->configuration['path_pattern']))
      ->execute()
      ->fetchField();

    if ($path_alias) {
      // @TODO Rewrite on 8.8.0 https://www.drupal.org/node/3013865
      $path_alias_storage = \Drupal::service('path.alias_storage'); /** @var AliasStorageInterface $path_alias_storage */
      if (!$path_alias_storage->aliasExists('/' . $path_alias, 'ru')) {
        return [
          'alias' => '/' . $path_alias,
          'pathauto' => FALSE,
        ];
      }
    }
  }

}
