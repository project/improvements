<?php

namespace Drupal\improvements;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\druhels\ArrayHelper;
use Drupal\druhels\EntityHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Twig extensions.
 */
class ImprovementsTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('preg_matches', [$this, 'pregMatches']),
      new TwigFunction('field_has_value', [$this, 'fieldHasValue']),
      new TwigFunction('referenced_entities_field_has_value', [$this, 'referencedEntitiesFieldHasValue']),
      new TwigFunction('set_array_value', [$this, 'setArrayValue']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {
    return [
      new TwigFilter('without_theme', [$this, 'withoutTheme']), // @TODO Add test
      new TwigFilter('relative_url', 'file_url_transform_relative'), // @TODO Add test
      new TwigFilter('filter_empty', [$this, 'filterEmpty']), // @TODO Add test
      new TwigFilter('unique', [$this, 'arrayUnique']), // @TODO Add test
      new TwigFilter('clean_phone', [$this, 'cleanPhone']), // @TODO Add test
    ];
  }

  /**
   * Call preg_match() and return matches.
   *
   * @return string|array|null
   */
  public function pregMatches(string $pattern, string $subject, int $match_index = NULL) {
    if (preg_match($pattern, $subject, $matches)) {
      if ($match_index !== NULL) {
        return $matches[$match_index] ?? NULL;
      }
      return $matches;
    }
    return NULL;
  }

  /**
   * Return render-array without #theme property.
   *
   * @return mixed
   */
  public function withoutTheme($element) {
    if (is_array($element) && isset($element['#theme'])) {
      unset($element['#theme']);
    }
    return $element;
  }

  /**
   * Return TRUE if field has value.
   *
   * @param FieldItemListInterface $field_items
   * @param mixed $value
   * @param string|null $value_property_name
   *
   * @TODO Add test
   */
  public function fieldHasValue(FieldItemListInterface $field_items, $value, string $value_property_name = NULL): bool {
    return EntityHelper::fieldHasValue($field_items, $value, $value_property_name);
  }

  /**
   * Return TRUE if referenced entities field has value.
   *
   * @param EntityReferenceFieldItemListInterface $parent_items
   * @param string $child_field_name
   * @param mixed $child_field_value
   * @param string|null $child_value_property_name
   *
   * @TODO Add test
   */
  public function referencedEntitiesFieldHasValue(EntityReferenceFieldItemListInterface $parent_items, string $child_field_name, $child_field_value, string $child_value_property_name = NULL): bool {
    return EntityHelper::referencedEntitiesFieldHasValue($parent_items, $child_field_name, $child_field_value, $child_value_property_name);
  }

  /**
   * Set array value.
   *
   * @TODO Add test
   */
  public function setArrayValue($array, $keys, $value): array {
    if (!is_array($keys)) {
      $keys = [$keys];
    }

    NestedArray::setValue($array, $keys, $value);

    return $array;
  }

  /**
   * Remove empty elements from array.
   */
  public function filterEmpty($array) {
    if (is_array($array)) {
      $array = ArrayHelper::removeEmptyElements($array);
    }
    return $array;
  }

  /**
   * Remove duplicate values.
   */
  public function arrayUnique($array) {
    if (is_array($array)) {
      return array_unique($array);
    }
    return $array;
  }

  /**
   * Clean phone.
   */
  public function cleanPhone(string $phone): string {
    return preg_replace('/[^0-9+]/', '', $phone);
  }

}
