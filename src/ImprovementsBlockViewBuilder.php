<?php

namespace Drupal\improvements;

use Drupal\block\BlockInterface;
use Drupal\block\BlockViewBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;

class ImprovementsBlockViewBuilder extends BlockViewBuilder {

  /**
   * {@inheritDoc}
   *
   * Change method visibillity.
   */
  public static function buildPreRenderableBlock(BlockInterface $entity, ModuleHandlerInterface $module_handler): array {
    return parent::buildPreRenderableBlock($entity, $module_handler);
  }

}
