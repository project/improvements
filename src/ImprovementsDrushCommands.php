<?php

namespace Drupal\improvements;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\search\SearchPageRepositoryInterface;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;

class ImprovementsDrushCommands extends DrushCommands {

  /**
   * @command improvements:route-info
   * @aliases route-info,ri
   * @param string $path Path
   */
  public function routeInfo(string $path): void {
    $request = Request::create($path);
    $router = \Drupal::service('router.no_access_checks');
    try {
      $route = $router->matchRequest($request);
      $this->io()->text(print_r($route, TRUE));
    }
    catch (\Exception $e) {
      $this->io()->error('No route has been found.');
    }
  }

  /**
   * @command improvements:search-index
   * @aliases search-index
   */
  public function searchIndex(): void {
    while (($remaining = $this->getSearchRemaining()) > 0) {
      $this->io()->text('Remaining: ' . $remaining);
      search_cron();
    }
    $this->io()->success('Indexing is complete.');
  }

  /**
   * Return seach remaining.
   */
  protected function getSearchRemaining(): int {
    $remaining = 0;
    $search_page_repository = \Drupal::service('search.search_page_repository'); /** @var $search_page_repository SearchPageRepositoryInterface */
    foreach ($search_page_repository->getIndexableSearchPages() as $search_page) {
      if ($search_page->isIndexable() && ($search_page_status = $search_page->getPlugin()->indexStatus())) {
        $remaining += $search_page_status['remaining'];
      }
    }

    return $remaining;
  }

  /**
   * @command improvements:entity-query
   * @aliases entity-query
   * @param string $entity_type Entity type id
   * @param string $conditions Conditions in uri format
   * @param string $no_result_message No result message
   */
  public function entityQuery(string $entity_type, string $conditions, string $no_result_message = 'none'): void {
    $query = \Drupal::entityQuery($entity_type);
    $query->accessCheck(FALSE);

    $parsed_conditions = HeaderUtils::parseQuery($conditions);
    foreach ($parsed_conditions as $name => $value) {
      $query->condition($name, (array)$value, 'IN');
    }

    try {
      $result = $query->execute();
      $this->io()->write($result ? implode(',', array_keys($result)) : $no_result_message);
    }
    catch (\Exception $exception) {
      $this->io()->write($no_result_message);
      throw $exception;
    }
  }

  /**
   * @hook pre-command queue:run
   */
  public function preQueueRun(CommandData $commandData): void {
    $queue_name = $commandData->input()->getArgument('name');
    $queue = \Drupal::queue($queue_name);

    \Drupal::logger('drush')->debug('Queue "@name" processing started. Number of items = @number', [
      '@name' => $queue_name,
      '@number' => $queue->numberOfItems(),
    ]);
  }

  /**
   * @hook post-command queue:run
   */
  public function postQueueRun($result, CommandData $commandData): void {
    \Drupal::logger('drush')->debug('Queue "@name" processing finished.', [
      '@name' => $commandData->input()->getArgument('name'),
    ]);
  }

  /**
   * @hook pre-command pm:uninstall
   */
  public function prePmUninstall(CommandData $commandData) {
    $arguments = $commandData->input()->getArguments();
    $target_modules = StringUtils::csvToArray($arguments['modules']);
    $exists_target_modules = array_filter($target_modules, function ($module) {
      return \Drupal::moduleHandler()->moduleExists($module);
    });

    if (!$exists_target_modules) {
      $this->logger()->warning(dt('No modules to uninstall.'));
      return CommandResult::exitCode(0);
    }

    return null;
  }

}
