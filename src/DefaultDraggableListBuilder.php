<?php

namespace Drupal\improvements;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

class DefaultDraggableListBuilder extends DraggableListBuilder {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'default_draggable_config_list';
  }

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    $header = [
      'id' => $this->t('ID'),
      'label' => $this->t('Label'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['id']['#markup'] = $entity->id();
    $row['label'] = $entity->label();

    return $row + parent::buildRow($entity);
  }

}
