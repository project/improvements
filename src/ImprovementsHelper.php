<?php

namespace Drupal\improvements;

use Drupal\druhels\EntityHelper;
use Drupal\druhels\TaxonomyHelper;

class ImprovementsHelper {

  /**
   * Return textarea rows count by text length.
   */
  public static function getTextareaRowsCount(string $text, int $min_rows): int {
    $rows = substr_count($text, "\n");
    $rows = min($rows + 1, 28);
    return max($rows, $min_rows);
  }

  /**
   * Return e-mail for notifications.
   */
  public static function getEmailForNotifications(): string {
    $site_info = improvements_get_site_information();
    return $site_info['mail_for_notifications'] ?: $site_info['mail'];
  }

  /**
   * Return biild time.
   */
  public static function getBuildTime(): string {
    static $build_time;

    if ($build_time === NULL) {
      $build_time = date(DATE_ATOM, \Drupal::time()->getCurrentTime());
    }

    return $build_time;
  }

  /**
   * Invoke hook from themes.
   */
  public static function invokeAllFromThemes(string $hook, array $args = []): array {
    static $themes;

    if ($themes === NULL) {
      $active_theme = \Drupal::theme()->getActiveTheme();
      $themes = array_keys($active_theme->getBaseThemeExtensions());
      $themes[] = $active_theme->getName();
    }

    $results = [];

    foreach ($themes as $theme_name) {
      $function = $theme_name . '_' . $hook;
      if (function_exists($function)) {
        $result = $function(...$args);
        if ($result !== NULL) {
          $results[] = $result;
        }
      }
    }

    return $results;
  }

  /**
   * Return term all parents field values.
   */
  public static function getTermAllParentsFieldValues(int $term_id, string $field_name, string $property_name = NULL): array {
    $all_parents = TaxonomyHelper::getAllParentsTerms($term_id, FALSE);
    $field_values = [];

    foreach ($all_parents as $parent_term) {
      if (
        $parent_term->hasField($field_name) &&
        ($field_items = $parent_term->get($field_name)) &&
        !$field_items->isEmpty()
      ) {
        $field_values = array_merge($field_values, EntityHelper::getFieldValues($field_items, $property_name));
      }
    }

    return $field_values;
  }

  /**
   * Format array as attribites.
   */
  public static function formatArrayAsAttributes(array $array): array {
    $attributes = [];

    foreach ($array as $attribute_name => $attribute_value) {
      if ($attribute_name == 'class') {
        $attributes[$attribute_name] = [$attribute_value];
      }
      else {
        $attributes[$attribute_name] = $attribute_value;
      }
    }

    return $attributes;
  }

}
